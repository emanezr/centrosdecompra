<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return redirect('/dashboard');
});

Route::group(['middleware' => ['auth']], function() {

    Route::get('/dashboard', 'HomeController@index')->name('dashboard');

    //Users Route
    Route::get('/usuarios', 'UserController@index')->name('users.index');
    Route::post('/usuarios', 'UserController@store');
    Route::get('/usuarios/nuevo', 'UserController@create');
    Route::get('/usuarios/{user}', 'UserController@show')
        ->where('id', '[0-9]+')->name('users.show');
    Route::get('/usuarios/{user}/editar', 'UserController@edit')->name('users.edit');
    Route::put('/usuarios/{user}', 'UserController@update');
    Route::delete('/usuarios/{user}', 'UserController@destroy')->name('users.destroy');
    Route::get('/usuarios/data/datatables', 'UserController@dataUsers');
    //Reportes
    Route::get('/reportes/tablas/users', 'UserController@ReporteIndex')->name('reportes.users.index');
    Route::get('/reportes/tablas/users/printpdf', 'UserController@ListadoPDF')->name('reportes.users.printpdf');


    //Sucursal
    Route::get('/sucursales', 'SucursalController@index')->name('sucursales.index');
    Route::post('/sucursales', 'SucursalController@store');
    Route::get('/sucursales/data/datatables', 'SucursalController@dataSucursales');
    Route::get('/sucursales/nuevo', 'SucursalController@create');
    Route::get('/sucursales/{sucursal}', 'SucursalController@show')
        ->name('sucursales.show');
    Route::get('/sucursales/{sucursal}/editar', 'SucursalController@edit')
        ->name('sucursales.edit');
    Route::put('/sucursales/{sucursal}', 'SucursalController@update');
    Route::delete('/sucursales/{sucursal}', 'SucursalController@destroy')->name('sucursales.destroy');
    //Reportes
    Route::get('/reportes/tablas/sucursales', 'SucursalController@ReporteIndex')->name('reportes.sucursales.index');
    Route::get('/reportes/tablas/sucursales/printpdf', 'SucursalController@ListadoPDF')->name('reportes.sucursales.printpdf');


    //Bancos Route
    Route::get('/bancos', 'BancoController@index')->name('bancos.index');
    Route::post('/bancos', 'BancoController@store');
    Route::get('/bancos/nuevo', 'BancoController@create');
    Route::get('/bancos/{banco}', 'BancoController@show')
        ->where('id', '[0-9]+')->name('bancos.show');
    Route::get('/bancos/{banco}/editar', 'BancoController@edit')->name('bancos.edit');
    Route::put('/bancos/{banco}', 'BancoController@update');
    Route::delete('/bancos/{banco}', 'BancoController@destroy')->name('bancos.destroy');
    Route::get('/bancos/data/datatables', 'BancoController@dataBancos');
    //Reportes
    Route::get('/reportes/tablas/bancos', 'BancoController@ReporteIndex')->name('reportes.bancos.index');
    Route::get('/reportes/tablas/bancos/printpdf', 'BancoController@ListadoPDF')->name('reportes.bancos.printpdf');

    //Productos Route
    Route::get('/productos', 'ProductoController@index')->name('productos.index');
    Route::post('/productos', 'ProductoController@store');
    Route::get('/productos/nuevo', 'ProductoController@create');
    Route::get('/productos/{producto}', 'ProductoController@show')->name('productos.show');
    Route::get('/productos/{producto}/editar', 'ProductoController@edit')->name('productos.edit');
    Route::put('/productos/{producto}', 'ProductoController@update');
    Route::delete('/productos/{producto}', 'ProductoController@destroy')->name('productos.destroy');
    Route::get('/productos/data/datatables', 'ProductoController@dataProductos');
    //Reportes
    Route::get('/reportes/tablas/productos', 'ProductoController@ReporteIndex')->name('reportes.productos.index');
    Route::get('/reportes/tablas/productos/printpdf', 'ProductoController@ListadoPDF')->name('reportes.productos.printpdf');

    //Productores Route
    Route::get('/productores', 'ProductorController@index')->name('productores.index');
    Route::post('/productores', 'ProductorController@store');
    Route::get('/productores/nuevo', 'ProductorController@create');
    Route::get('/productores/{productor}', 'ProductorController@show')->name('productores.show');
    Route::get('/productores/{productor}/editar', 'ProductorController@edit')->name('productores.edit');
    Route::put('/productores/{productor}', 'ProductorController@update');
    Route::delete('/productores/{productor}', 'ProductorController@destroy')->name('productores.destroy');
    Route::get('/productores/data/datatables', 'ProductorController@dataProductores');
    //Reportes
    Route::get('/reportes/tablas/productores', 'ProductorController@ReporteIndex')->name('reportes.productores.index');
    Route::get('/reportes/tablas/productores/printpdf', 'ProductorController@ListadoPDF')->name('reportes.productores.printpdf');
    
    //Beneficiarios Route
    Route::get('/beneficiarios', 'BeneficiarioController@index')->name('beneficiarios.index');
    Route::post('/beneficiarios', 'BeneficiarioController@store');
    Route::get('/beneficiarios/nuevo', 'BeneficiarioController@create');
    Route::get('/beneficiarios/{beneficiario}', 'BeneficiarioController@show')
        ->where('id', '[0-9]+')->name('beneficiarios.show');
    Route::get('/beneficiarios/{beneficiario}/editar', 'BeneficiarioController@edit')->name('beneficiarios.edit');
    Route::put('/beneficiarios/{beneficiario}', 'BeneficiarioController@update');
    Route::delete('/beneficiarios/{beneficiario}', 'BeneficiarioController@destroy')->name('beneficiarios.destroy');
    Route::get('/beneficiarios/data/datatables', 'BeneficiarioController@dataBeneficiarios');
    //Reportes
    Route::get('/reportes/tablas/beneficiarios', 'BeneficiarioController@ReporteIndex')->name('reportes.beneficiarios.index');
    Route::get('/reportes/tablas/beneficiarios/printpdf', 'BeneficiarioController@ListadoPDF')->name('reportes.beneficiarios.printpdf');

    //Cajas Route
    Route::get('/cajas', 'CajaController@index')->name('cajas.index');
    Route::post('/cajas', 'CajaController@store');
    Route::get('/cajas/nuevo', 'CajaController@create');
    Route::get('/cajas/{caja}', 'CajaController@show')->name('cajas.show');
    Route::get('/cajas/{caja}/editar', 'CajaController@edit')->name('cajas.edit');
    Route::put('/cajas/{caja}', 'CajaController@update');
    Route::delete('/cajas/{caja}', 'CajaController@destroy')->name('cajas.destroy');
    Route::get('/cajas/data/datatables', 'CajaController@dataCajas');
    //Reportes
    //Route::get('/reportes/tablas/cajas', 'CajaController@ReporteIndex')->name('reportes.cajas.index');
    //Route::get('/reportes/tablas/cajas/printpdf', 'CajaController@ListadoPDF')->name('reportes.cajas.printpdf');

    //Variable Route
    Route::get('/variables', 'VariableController@index')->name('variables.index');
    Route::post('/variables', 'VariableController@store');
    Route::get('/variables/editar', 'VariableController@edit')->name('variables.edit');
    Route::put('/variables', 'VariableController@update');
    //Reportes
    Route::get('/reportes/tablas/variables', 'VariableController@ReporteIndex')->name('reportes.variables.index');
    Route::get('/reportes/tablas/variables/printpdf', 'VariableController@ListadoPDF')->name('reportes.variables.printpdf');

    //Recepciones Route
    Route::get('/recepciones', 'RecepcionController@index')->name('recepciones.index');
    Route::post('/recepciones', 'RecepcionController@store');

});



