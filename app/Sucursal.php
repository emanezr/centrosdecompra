<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sucursal extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'nombre', 'tlf', 'dir', 'estado_id', 'status'
    ];

    public $incrementing = false;

    public static function getAll()
    {
        return static::where('status', 'activo')->get();
    }
    
    /**
     * Relacion Sucursal - usuarios
     * 
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }

    /**
     * Relacion Sucursal - usuarios
     * 
     */
    public function productores()
    {
        return $this->hasMany(Productor::class);
    }

    /**
     * Relacion Sucursal - Cajas
     * 
     */
    public function cajas()
    {
        return $this->hasMany(Caja::class);
    }

    /**
     * Relacion Sucursal - Estados
     * 
     */
    public function estado()
    {
        return $this->belongsTo(Estado::class);
    }
}
