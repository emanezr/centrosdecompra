<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Recepcion;

class RecepcionController extends Controller
{
    public function index()
    {
        return view('recepciones.index');
    }

    public function store()
    {
        $data = [
            'numero' => '123456',
            'fecha' => '2018-05-27 00:57:43',
            'status' => 'cerrado',
            'qqneto' => 150,
            'kgneto' => 1500,
            'qqporpagar' => 100,
            'kgporpagar' => 1000,
            'promedioqq' => 45,
            'caleta' => 300,
            'totalcaleta' => 1000,
            'cantsacos' => 50,
            'totaltara' => 150,
            'obsrvacion' => 'TEXT TEXT TEXT',
            'productor_id' => '11111111',
            'sucursal_id' => 'arau',
            'user_id' => '17341248',
            'producto_id' => 'AA1122',
        ];
    }
}