<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use App\Producto;
use App\User;

use Barryvdh\DomPDF\Facade as PDF;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {

        $this->middleware('permission:producto-list', ['only' => ['index','dataProductos']]);
        $this->middleware('permission:producto-create', ['only' => ['create','store']]);
        $this->middleware('permission:producto-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:producto-delete', ['only' => ['destroy']]);
        $this->middleware('permission:producto-report', ['only' => ['ReporteIndex', 'ListadoPDF']]);
    }

    public function index()
    {
        return view('productos.index');
    }

    public function show(Producto $producto)
    {   
        if ($producto->status == "activo") {
            return view('productos.show', compact('producto'));
        }
        return redirect('productos')
                ->with('status', 'Aviso!')
                ->with('alert' , 'danger')
                ->with('mensaje', 'El producto se ha eliminado o esta inactivo');
    }

    public function create()
    {   
        return view('productos.create');
    }

    public function store()
    {

        $data = request()->validate(
            [
                'id' => ['required', 'unique:productos,id'],
                'nombre' => 'required',
                'precioqq' => ['required','numeric'],
                'descripcion' => 'required',
            ],
            [
                'nombre.required' => 'El campo nombre es obligatorio'
            ]
        );

        Producto::create([
            'id'=> $data['id'],
            'nombre' => $data['nombre'],
            'precioqq' => $data['precioqq'],
            'descripcion' => $data['descripcion'],
            'user_id' => Auth::user()->id,
        ]);

        return redirect()->route('productos.index')
            ->with('status', 'Creado!')
            ->with('alert' , 'success')
            ->with('mensaje', 'El producto se ha creado exitosamente.');
    }

    public function edit(Producto $producto)
    {
        return view('productos.edit', compact('producto'));   
    }
    
    public function update(Producto $producto)
    {
        $data = request()->validate(
            [
                'nombre' => 'required',
                'precioqq' => ['required','numeric'],
                'descripcion' => 'required',
            ],
            [
                'nombre.required' => 'El campo nombre es obligatorio'
            ]
        );
        
        $producto->update($data);
        return redirect()->route('productos.show', ['producto' => $producto])
            ->with('success', "El producto {$producto->nombre} se ha actualizado exitosamente");

    }

    public function destroy(Producto $producto)
    {
        $data = ['status' => 'inactivo'];
        
        $producto->update($data);
        return redirect()->route('productos.index')
            ->with('status', 'Eliminado')
            ->with('alert', 'danger')
            ->with('mensaje', 'El producto se ha eliminado exitosamente');
    }

    public function dataProductos()
    {
        $productos = Producto::select()->where('status', '=', 'activo');

        return Datatables::of($productos)
            ->addColumn(
                'acciones', function ($producto) {
                    return view('productos.actions', compact('producto'));
                }
            )
            ->rawColumns(['acciones'])
            ->make(true);
    }


    public function ReporteIndex()
    {
        $users = User::select()->where('status', 'activo')->get();
        $productos = Producto::select()->where('status', 'activo')->get();

        return view('reportes.productos.index', compact('productos', 'users'));

    }
    

    public function ListadoPDF()
    {
        $users = User::select()->where('status', 'activo')->get();
        $productos = Producto::select()->where('status', 'activo')->get();

        $pdf = PDF::loadView('reportes.productos.printpdf', compact('productos', 'users'));
        $filePDF = "productos-" . date("Ymdhis") . ".pdf";

        return $pdf->stream($filePDF);

    }
}
