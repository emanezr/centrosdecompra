<?php

namespace App\Http\Controllers;

use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\User;
use App\Sucursal;

use Barryvdh\DomPDF\Facade as PDF;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {

        $this->middleware('permission:user-list', ['only' => ['index','dataUsers']]);
        $this->middleware('permission:user-create', ['only' => ['create','store']]);
        $this->middleware('permission:user-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:user-delete', ['only' => ['destroy']]);
        $this->middleware('permission:user-report', ['only' => ['ReporteIndex', 'ListadoPDF']]);

    }

    public function index()
    {
        return view('users.index');
    }

    public function show(User $user)
    {   
        if ($user->status == "activo") {
            return view('users.show', compact('user'));
        }
        return redirect('usuarios')
                ->with('status', 'Aviso!')
                ->with('alert' , 'danger')
                ->with('mensaje', 'El usuario se ha eliminado o esta inactivo');
    }

    public function create()
    {   
        $sucursales = Sucursal::select('id', 'nombre')->get();
        //dd($sucursales);
        return view('users.create', compact('sucursales'));
    }

    public function store()
    {
        $data = request()->validate(
            [
                'id' => ['required', 'unique:users,id'],
                'nombre' => 'required',
                'tlf' => 'required',
                'dir' => 'required',
                'nivel' => ['required', 'in:Usuario,Auditor,Administrador'],
                'sucursal_id' => 'required',
                'password' => 'required|string|min:6|confirmed',
            ],
            [
                'nombre.required' => 'El campo nombre es obligatorio'
            ]
        );

        $user = User::create([
            'id'=> $data['id'],
            'nombre' => $data['nombre'],
            'tlf' => $data['tlf'],
            'dir' => $data['dir'],
            'nivel' => $data['nivel'],
            'sucursal_id' => $data['sucursal_id'],
            'password' => bcrypt($data['password'])
        ]);
        
        if ($user->nivel == 'Administrador') {
            $user->assignRole('Administrador');
        }
        elseif ($user->nivel == 'Auditor') {
            $user->assignRole('Auditor');
        }
        else {
            $user->assignRole('Usuario');
        }
        
        return redirect()->route('users.index')
            ->with('status', 'Creado!')
            ->with('alert' , 'success')
            ->with('mensaje', 'El usuario se ha creado exitosamente.');
    }

    public function edit(User $user)
    {
        $sucursales = Sucursal::select('id', 'nombre')->get();
        return view('users.edit', compact('user', 'sucursales'));   
    }
    
    public function update(User $user)
    {
        $data = request()->validate(
            [
                'nombre' => 'required',
                'tlf' => 'required',
                'dir' => 'required',
                'nivel' => ['required', 'in:Usuario,Auditor,Administrador'],
                'sucursal_id' => 'required',
                'password' => 'confirmed',
            ],
            [
                'nombre.required' => 'El campo nombre es obligatorio'
            ]
        );
        
        if ($data['password'] != null) {
            $data['password'] = bcrypt($data['password']);
        } else {
            unset($data['password']);
        }

        if ($user->nivel == 'Administrador') {
            $user->removeRole('Administrador');
        }
        elseif ($user->nivel == 'Auditor') {
            $user->removeRole('Auditor');
        }
        elseif ($user->nivel == 'Usuario') {
            $user->removeRole('Usuario');
        }

        $user->update($data);

        if ($user->nivel == 'Administrador') {
            $user->assignRole('Administrador');
        }
        elseif ($user->nivel == 'Auditor') {
            $user->assignRole('Auditor');
        }
        elseif ($user->nivel == 'Usuario') {
            $user->assignRole('Usuario');
        }

        return redirect()->route('users.show', ['user' => $user])
            ->with('success', "El usuario {$user->nombre} se ha actualizado exitosamente");

    }

    public function destroy(User $user)
    {
        $data = ['status' => 'inactivo'];
        
        $user->update($data);
        
        return redirect()->route('users.index')
            ->with('status', 'Eliminado')
            ->with('alert' , 'danger')
            ->with('mensaje', 'El usuario se ha eliminado exitosamente');
    }

    public function dataUsers()
    {
        $users = User::select()->where('status', 'activo')->get();

        return Datatables::of($users)          
            ->addColumn(
                'sucursal', function ($user) {
                    return $user->sucursal->nombre;
                }
            )
            ->addColumn(
                'acciones', function ($user) {
                    return view('users.actions', compact('user'));
                }
            )
            ->rawColumns(['acciones'])
            ->make(true);
    }
    
    public function ReporteIndex()
    {
        $sucursales = Sucursal::select()->where('status', '=', 'activo')->get();
        $sucursal_id = request()->input('sucursal_id');
        $nivel = request()->input('nivel');
        //dd($nivel);
        if(request()->has(['sucursal_id', 'nivel'])){

            $users = User::select()->where('status', '=', 'activo')->orderBy('id', 'DESC')
            ->sucursal($sucursal_id)
            ->nivel($nivel)
            ->paginate(20); 

            return view('reportes.users.index', compact('sucursales', 'users', 'sucursal_id', 'nivel'));

        } else {

            $sucursal_id = "";
            $nivel = "";
            $users = User::select()->where('status', '=', 'activo')->orderBy('id', 'ASC')->paginate(20); 
            $sucursales = Sucursal::select()->where('status', '=', 'activo')->get();

            return view('reportes.users.index', compact('sucursales', 'users', 'sucursal_id', 'nivel'));
        }

    }
    

    public function ListadoPDF()
    {
        //$users = "";
        $sucursalpdf = request()->input('sucursalpdf');
        $nivelpdf = request()->input('nivelpdf');
        //dd($nivel);
        if(request()->has('sucursalpdf') or request()->has('nivelpdf')){

            $users = User::select()->where('status', '=', 'activo')->orderBy('id', 'DESC')
            ->sucursal($sucursalpdf)
            ->nivel($nivelpdf)
            ->get();

        } else {

            $users = User::select()->where('status', '=', 'activo')->orderBy('sucursal_id', 'ASC')->get(); 

        }

        $pdf = PDF::loadView('reportes.users.printpdf', compact('users')); 
        $filePDF = "usuarios-" . date("Ymdhis") . ".pdf";

        return $pdf->stream($filePDF);

    }
}
