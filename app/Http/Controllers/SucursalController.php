<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Sucursal;
use App\Estado;

use Barryvdh\DomPDF\Facade as PDF;

class SucursalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:sucursal-list', ['only' => ['index','dataSucursales']]);
        $this->middleware('permission:sucursal-create', ['only' => ['create','store']]);
        $this->middleware('permission:sucursal-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:sucursal-delete', ['only' => ['destroy']]);
        $this->middleware('permission:sucursal-report', ['only' => ['ReporteIndex', 'ListadoPDF']]);

    }
    
    public function index()
    {
        return view('sucursales.index');
    }

    public function show(Sucursal $sucursal)
    {   
        if ($sucursal->status == "activo") {
            return view('sucursales.show', compact('sucursal'));
        }
        return redirect('sucursales')
                ->with('status', 'Aviso!')
                ->with('alert', 'danger')
                ->with('mensaje', 'La sucursal se ha eliminado o esta inactiva');
    }

    public function create()
    {   
        $estados = Estado::select('id', 'nombre')->get();
        //dd($sucursales);
        return view('sucursales.create', compact('estados'));
    }

    public function store()
    {
        $data = request()->validate(
            [
                'id' => ['required', 'unique:sucursals,id'],
                'nombre' => 'required',
                'tlf' => 'required',
                'dir' => 'required',
                'estado_id' => 'required',
            ],
            [
                'nombre.required' => 'El campo nombre es obligatorio'
            ]
        );

        Sucursal::create([
            'id'=> $data['id'],
            'nombre' => $data['nombre'],
            'tlf' => $data['tlf'],
            'dir' => $data['dir'],
            'estado_id' => $data['estado_id'],
        ]);
        return redirect()->route('sucursales.index')
            ->with('status', 'Creado!')
            ->with('alert' , 'success')
            ->with('mensaje', 'La sucursal se ha creado exitosamente.');
    }

    public function edit(Sucursal $sucursal)
    {
        $estados = Estado::select('id', 'nombre')->get();
        return view('sucursales.edit', compact('sucursal', 'estados'));
    }

    public function update(Sucursal $sucursal)
    {
        $data = request()->validate(
            [
                'nombre' => 'required',
                'tlf' => 'required',
                'dir' => 'required',
                'estado_id' => 'required',
            ],
            [
                'nombre.required' => 'El campo nombre es obligatorio'
            ]
        );
        
        $sucursal->update($data);
        return redirect()->route('sucursales.show', ['sucursal' => $sucursal])
            ->with('success', "La sucursal {$sucursal->nombre} se ha actualizado exitosamente");

    }

    public function destroy(Sucursal $sucursal)
    {
        $data = ['status' => 'inactivo'];
        
        $sucursal->update($data);
        return redirect()->route('sucursales.index')
            ->with('status', 'Eliminado')
            ->with('alert' , 'danger')
            ->with('mensaje', 'La sucursal se ha eliminado exitosamente');
    }

    public function dataSucursales()
    {
        $sucursales = Sucursal::select()->where('status', '=', 'activo');

        return Datatables::of($sucursales)
            ->addColumn(
                'estado', function ($sucursal) {
                    return $sucursal->estado->nombre;
                }
            )      
            ->addColumn(
                'acciones', function ($sucursal) {
                    return view('sucursales.actions', compact('sucursal'));
                }
            )
            ->rawColumns(['acciones'])
            ->make(true);
    }

    public function ReporteIndex()
    {
        $estados = Estado::select('id', 'nombre')->get();
        $sucursales = Sucursal::select()->where('status', 'activo')->get();
        //dd($sucursales);
        return view('reportes.sucursales.index', compact('estados', 'sucursales'));

    }
    

    public function ListadoPDF()
    {
        $estados = Estado::select('id', 'nombre')->get();
        $sucursales = Sucursal::select()->where('status', 'activo')->get();

        $pdf = PDF::loadView('reportes.sucursales.printpdf', compact('estados', 'sucursales'));
        $filePDF = "sucursales-" . date("Ymdhis") . ".pdf";

        return $pdf->stream($filePDF);

    }
}
