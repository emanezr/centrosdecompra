<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Banco;

use Barryvdh\DomPDF\Facade as PDF;


class BancoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {

        $this->middleware('permission:banco-list', ['only' => ['index','dataBancos']]);
        $this->middleware('permission:banco-create', ['only' => ['create','store']]);
        $this->middleware('permission:banco-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:banco-delete', ['only' => ['destroy']]);
        $this->middleware('permission:banco-report', ['only' => ['ReporteIndex', 'ListadoPDF']]);
    }

    public function index()
    {
        return view('bancos.index');
    }

    public function show(Banco $banco)
    {   
        if ($banco->status == "activo") {
            return view('bancos.show', compact('banco'));
        }
        return redirect('bancos')
                ->with('status', 'Aviso!')
                ->with('alert' , 'danger')
                ->with('mensaje', 'El banco se ha eliminado o esta inactivo');
    }

    public function create()
    {   
        return view('bancos.create');
    }

    public function store()
    {
        $data = request()->validate(
            [
                'id' => ['required', 'unique:bancos,id', 'max:4'],
                'nombre' => 'required',
            ],
            [
                'nombre.required' => 'El campo nombre es obligatorio'
            ]
        );

        Banco::create([
            'id'=> $data['id'],
            'nombre' => $data['nombre']
        ]);
        return redirect()->route('bancos.index')
            ->with('status', 'Creado!')
            ->with('alert' , 'success')
            ->with('mensaje', 'El banco se ha creado exitosamente.');
    }

    public function edit(Banco $banco)
    {
        return view('bancos.edit', compact('banco'));   
    }
    
    public function update(Banco $banco)
    {
        $data = request()->validate(
            ['nombre' => 'required'],
            ['nombre.required' => 'El campo nombre es obligatorio']
        );

        $banco->update($data);
        return redirect()->route('bancos.show', ['banco' => $banco])
            ->with('success', "El banco {$banco->nombre} se ha actualizado exitosamente");

    }

    public function destroy(Banco $banco)
    {
        $data = ['status' => 'inactivo'];
        
        $banco->update($data);
        return redirect()->route('bancos.index')
            ->with('status', 'Eliminado')
            ->with('alert' , 'danger')
            ->with('mensaje', 'El banco se ha eliminado exitosamente');
    }

    public function dataBancos()
    {
        $bancos = Banco::select()->where('status', '=', 'activo');

        return Datatables::of($bancos)          
            ->addColumn(
                'acciones', function ($banco) {
                    return view('bancos.actions', compact('banco'));
                }
            )
            ->rawColumns(['acciones'])
            ->make(true);
    }

    public function ReporteIndex()
    {
        $bancos = Banco::select()->where('status', 'activo')->get();

        return view('reportes.bancos.index', compact('bancos'));

    }
    

    public function ListadoPDF()
    {
        $bancos = Banco::select()->where('status', 'activo')->get();

        $pdf = PDF::loadView('reportes.bancos.printpdf', compact('bancos'));
        $filePDF = "bancos-" . date("Ymdhis") . ".pdf";

        return $pdf->stream($filePDF);

    }
}
