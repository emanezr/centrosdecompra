<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GeneralUseController extends Controller
{
    public function sucursalSession($sucursal)
    {
        $request->session()->put('SucursalSession', $sucursal);
    }
}
