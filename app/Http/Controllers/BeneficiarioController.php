<?php

namespace App\Http\Controllers;

use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Banco;
use App\Beneficiario;
use Barryvdh\DomPDF\Facade as PDF;

class BeneficiarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {

        $this->middleware('permission:beneficiario-list', ['only' => ['index','dataUsers']]);
        $this->middleware('permission:beneficiario-create', ['only' => ['create','store']]);
        $this->middleware('permission:beneficiario-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:beneficiario-delete', ['only' => ['destroy']]);
        $this->middleware('permission:beneficiario-report', ['only' => ['ReporteIndex', 'ListadoPDF']]);
    }

    public function index()
    {
        return view('beneficiarios.index');
    }

    public function show(Beneficiario $beneficiario)
    {   
        if ($beneficiario->status == "activo") {
            return view('beneficiarios.show', compact('beneficiario'));
        }
        return redirect('beneficiarios')
                ->with('status', 'Aviso!')
                ->with('alert' , 'danger')
                ->with('mensaje', 'El beneficiario se ha eliminado o esta inactivo');
    }

    public function create()
    {   
        $bancos = Banco::select('id', 'nombre')->get();
        //dd($sucursales);
        return view('beneficiarios.create', compact('bancos'));
    }

    public function store()
    {
        $data = request()->validate(
            [
                'id' => ['required', 'unique:beneficiarios,id'],
                'nombre' => 'required',
                'tipocta' => ['required', 'in:Corriente,Ahorro'],
                'cta' => 'required',
                'correo' => ['required', 'email'],
                'observacion' => 'string',
                'banco_id' => 'required',
            ],
            [
                'nombre.required' => 'El campo nombre es obligatorio'
            ]
        );

        $beneficiario = Beneficiario::create([
            'id'=> $data['id'],
            'nombre' => $data['nombre'],
            'tipocta' => $data['tipocta'],
            'cta' => $data['cta'],
            'correo' => $data['correo'],
            'observacion' => $data['observacion'],
            'banco_id' => $data['banco_id'],
        ]);
 
        return redirect()->route('beneficiarios.index')
            ->with('status', 'Creado!')
            ->with('alert' , 'success')
            ->with('mensaje', 'El beneficiario se ha creado exitosamente.');
    }

    public function edit(Beneficiario $beneficiario)
    {
        $bancos = Banco::select('id', 'nombre')->get();
        return view('beneficiarios.edit', compact('beneficiario', 'bancos'));   
    }
    
    public function update(Beneficiario $beneficiario)
    {
        $data = request()->validate(
            [
                'nombre' => 'required',
                'tipocta' => ['required', 'in:Corriente,Ahorro'],
                'cta' => 'required',
                'correo' => ['required', 'email'],
                'observacion' => 'string',
                'banco_id' => 'required',
            ],
            [
                'nombre.required' => 'El campo nombre es obligatorio'
            ]
        );  

        $beneficiario->update($data);

        return redirect()->route('beneficiarios.show', ['beneficiario' => $beneficiario])
            ->with('success', "El beneficiario {$beneficiario->nombre} se ha actualizado exitosamente");

    }

    public function destroy(Beneficiario $beneficiario)
    {
        $data = ['status' => 'inactivo'];
        
        $beneficiario->update($data);

        return redirect()->route('beneficiarios.index')
            ->with('status', 'Eliminado')
            ->with('alert' , 'danger')
            ->with('mensaje', 'El beneficiario se ha eliminado exitosamente');
    }

    public function dataBeneficiarios()
    {
        $beneficiarios = Beneficiario::select()->where('status', 'activo')->get();
        //dd($beneficiarios);
        return Datatables::of($beneficiarios)          
            ->addColumn(
                'banco', function ($beneficiario) {
                    return $beneficiario->banco->nombre;
                }
            )
            ->addColumn(
                'acciones', function ($beneficiario) {
                    return view('beneficiarios.actions', compact('beneficiario'));
                }
            )
            ->rawColumns(['acciones'])
            ->make(true);
    }
    

    public function ReporteIndex()
    {
        $beneficiarios = Beneficiario::all();
        return view('reportes.beneficiarios.index', compact('beneficiarios'));
    }
    

    public function ListadoPDF()
    {
        $beneficiarios = Beneficiario::all();

        $pdf = PDF::loadView('reportes.beneficiarios.printpdf', compact('beneficiarios')); 
        $filePDF = "beneficiarios-" . date("Ymdhis") . ".pdf";

        return $pdf->stream($filePDF);

    }

}
