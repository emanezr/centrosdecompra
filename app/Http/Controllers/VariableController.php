<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Variable;
use Barryvdh\DomPDF\Facade as PDF;

class VariableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:variable-list', ['only' => ['index']]);
        $this->middleware('permission:variable-create', ['only' => ['create','store']]);
        $this->middleware('permission:variable-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:variable-delete', ['only' => ['destroy']]);
        $this->middleware('permission:variable-report', ['only' => ['ReporteIndex', 'ListadoPDF']]);
    }
    public function index() 
    {
        $variable = Variable::find(1);
        
        return view('variables.index', compact('variable'));
    }

    public function edit() 
    {
        $variable = Variable::find(1);
        
        return view('variables.edit', compact('variable'));
    }

    public function update()
    {
        $variable = Variable::find(1);
        $data = request()->validate(
            [
                'kgqq' => ['required', 'numeric'],
                'porcentaje' => ['required', 'numeric'],
                'sacococuiza' => ['required', 'numeric'],
                'sacoplastico' => ['required', 'numeric'],
                'caletaqq' => ['required', 'numeric'],
                'caletakg' => ['required', 'numeric'],
            ],
            [
                'kgqq.required' => 'El campo es obligatorio'
            ]
        );
        
        $variable->update($data);
        return redirect()->route('variables.index')
            ->with('success', "Las variables se han actualizado exitosamente");

    }

    public function ReporteIndex()
    {
        $variables = Variable::all();
        return view('reportes.variables.index', compact('variables'));

    }
    

    public function ListadoPDF()
    {
        $variables = Variable::all();

        $pdf = PDF::loadView('reportes.variables.printpdf', compact('variables')); 
        $filePDF = "variables-" . date("Ymdhis") . ".pdf";

        return $pdf->stream($filePDF);

    }
}
