<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Productor;
use App\Estado;
use App\Sucursal;

use Barryvdh\DomPDF\Facade as PDF;


class ProductorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {

        $this->middleware('permission:productor-list', ['only' => ['index','dataProductores']]);
        $this->middleware('permission:productor-create', ['only' => ['create','store']]);
        $this->middleware('permission:productor-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:productor-delete', ['only' => ['destroy']]);
        $this->middleware('permission:productor-report', ['only' => ['ReporteIndex', 'ListadoPDF']]);
    }
    
    public function index()
    {
        return view('productores.index');
    }

    public function show(Productor $productor)
    {   

        if ($productor->status == "activo") {
            return view('productores.show', compact('productor'));
        }
        return redirect('productores')
                ->with('status', 'Aviso!')
                ->with('alert' , 'danger')
                ->with('mensaje', 'El productor se ha eliminado o esta inactivo');
    }

    public function create()
    {   
        $estados = Estado::select('id', 'nombre')->get();
        return view('productores.create', compact('estados'));
    }

    public function store()
    {

        $data = request()->validate(
            [
                'id' => ['required', 'unique:productors,id'],
                'nombre' => 'required',
                'tlf' => 'required',
                'dir' => 'required',
                'estado_id' => 'required',
            ],
            [
                'nombre.required' => 'El campo nombre es obligatorio'
            ]
        );

        Productor::create([
            'id'=> $data['id'],
            'nombre' => $data['nombre'],
            'tlf' => $data['tlf'],
            'dir' => $data['dir'],
            'estado_id' => $data['estado_id'],
        ]);

        return redirect()->route('productores.index')
            ->with('status', 'Creado!')
            ->with('alert' , 'success')
            ->with('mensaje', 'El productor se ha creado exitosamente.');
    }

    public function edit(Productor $productor)
    {
        $estados = Estado::select('id', 'nombre')->get();
        return view('productores.edit', compact('productor', 'estados'));   
    }
    
    public function update(Productor $productor)
    {
        $data = request()->validate(
            [
                'nombre' => 'required',
                'tlf' => 'required',
                'dir' => 'required',
                'estado_id' => 'required',
            ],
            [
                'nombre.required' => 'El campo nombre es obligatorio'
            ]
        );
        
        $productor->update($data);
        return redirect()->route('productores.show', ['productor' => $productor])
            ->with('success', "El productor {$productor->nombre} se ha actualizado exitosamente");

    }

    public function destroy(Productor $productor)
    {
        $data = ['status' => 'inactivo'];
        
        $productor->update($data);
        return redirect()->route('productores.index')
            ->with('status', 'Eliminado')
            ->with('alert', 'danger')
            ->with('mensaje', 'El productor se ha eliminado exitosamente');
    }

    public function dataProductores()
    {
        $productores = Productor::select()->where('status', 'activo')->get();

        return Datatables::of($productores)
            ->addColumn(
                'estado', function ($productor) {
                    return $productor->estado->nombre;
                }
            )
            ->addColumn(
                'acciones', function ($productor) {
                    return view('productores.actions', compact('productor'));
                }
            )
            ->rawColumns(['acciones'])
            ->make(true);
    }

    public function ReporteIndex()
    {
        $sucursales = Sucursal::select()->where('status', 'activo')->get();
        $estados = Estado::all();
        $sucursal_id = request()->input('sucursal_id');
        $estado_id = request()->input('estado_id');
        //dd($nivel);
        if(request()->has('sucursal_id') || request()->has('estado_id')){

            $productores = Productor::select()->where('status', 'activo')->orderBy('id', 'DESC')
            ->sucursal($sucursal_id)
            ->estado($estado_id)
            ->paginate(20); 

            return view('reportes.productores.index', compact('productores', 'sucursales', 'estados', 'sucursal_id', 'estado_id'));

        } else {

            $sucursal_id = "";
            $estado_id = "";
            $productores = Productor::select()->where('status', 'activo')->orderBy('id', 'DESC')->paginate(20);
            $sucursales = Sucursal::select()->where('status', 'activo')->get();

            return view('reportes.productores.index', compact('productores', 'sucursales', 'estados', 'sucursal_id', 'estado_id'));
        }

    }
    

    public function ListadoPDF()
    {

        $sucursales = Sucursal::select()->where('status', 'activo')->get();
        $estados = Estado::all();
        $sucursal_id = request()->input('sucursal_id');
        $estado_id = request()->input('estado_id');

        if(request()->has('sucursalpdf') || request()->has('estadopdf')){

            $productores = Productor::select()->where('status', 'activo')->orderBy('id', 'DESC')
            ->sucursal($sucursal_id)
            ->estado($estado_id)
            ->get();

        } else {

            $productores = Productor::select()->where('status', '=', 'activo')->orderBy('sucursal_id', 'ASC')->get(); 

        }

        $pdf = PDF::loadView('reportes.productores.printpdf', compact('productores')); 
        $filePDF = "productores-" . date("Ymdhis") . ".pdf";

        return $pdf->stream($filePDF);

    }
}
