<?php

namespace App\Http\Controllers;
use Yajra\Datatables\Datatables;
use App\Sucursal;
use App\Caja;

use Illuminate\Http\Request;

class CajaController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {

        $this->middleware('permission:caja-list', ['only' => ['index','dataCajas']]);
        $this->middleware('permission:caja-create', ['only' => ['create','store']]);
        $this->middleware('permission:caja-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:caja-delete', ['only' => ['destroy']]);
        $this->middleware('permission:caja-report', ['only' => ['ReporteIndex', 'ListadoPDF']]);
    }
    public function index()
    {
        return view('cajas.index');
    }

    public function show(Caja $caja)
    {   

        if ($caja->status == "activo") {
            return view('cajas.show', compact('caja'));
        }
        return redirect('cajas')
                ->with('status', 'Aviso!')
                ->with('alert' , 'danger')
                ->with('mensaje', 'La caja se ha eliminado o esta inactivo');
    }

    public function create()
    {   
        $sucursales = Sucursal::select('id', 'nombre')->get();
        return view('cajas.create', compact('sucursales'));
    }

    public function store()
    {

        $data = request()->validate(
            [
                'numero' => 'required',
                'descripcion' => 'required',
                'bs' => 'required',
                'observacion' => 'required',
                'sucursal_id' => 'required',
            ],
            [
                'numero.required' => 'El campo es obligatorio'
            ]
        );

        Caja::create([
            'numero' => $data['numero'],
            'descripcion' => $data['descripcion'],
            'bs' => $data['bs'],
            'observacion' => $data['observacion'],
            'sucursal_id' => $data['sucursal_id'],
        ]);

        return redirect()->route('cajas.index')
            ->with('status', 'Creado!')
            ->with('alert' , 'success')
            ->with('mensaje', 'La caja se ha creado exitosamente.');
    }

    public function edit(Caja $caja)
    {
        $sucursales = Sucursal::select('id', 'nombre')->get();
        return view('cajas.edit', compact('caja', 'sucursales'));   
    }
    
    public function update(Caja $caja)
    {
        $data = request()->validate(
            [
                'numero' => 'required',
                'descripcion' => 'required',
                'bs' => 'required',
                'observacion' => 'required',
                'sucursal_id' => 'required',
            ],
            [
                'numero.required' => 'El campo es obligatorio'
            ]
        );
        
        $caja->update($data);
        return redirect()->route('cajas.show', ['caja' => $caja])
            ->with('success', "La caja {$caja->nombre} se ha actualizado exitosamente");

    }

    public function destroy(Caja $caja)
    {
        $data = ['status' => 'inactivo'];
        
        $caja->update($data);
        return redirect()->route('cajas.index')
            ->with('status', 'Eliminado')
            ->with('alert', 'danger')
            ->with('mensaje', 'El caja se ha eliminado exitosamente');
    }

    public function dataCajas()
    {
        $cajas = Caja::select()->where('status', '=', 'activo');

        return Datatables::of($cajas)
            ->addColumn(
                'sucursal', function ($caja) {
                    return $caja->sucursal->nombre;
                }
            )
            ->addColumn(
                'acciones', function ($caja) {
                    return view('cajas.actions', compact('caja'));
                }
            )
            ->rawColumns(['acciones'])
            ->make(true);
    }
}
