<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'nombre', 'status', 'precioqq', 'descripcion', 'user_id'
    ];

    public $incrementing = false;

    
    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public function recepciones()
    {
        return $this->belongsToMany(Recepcion::class);
    }
}
