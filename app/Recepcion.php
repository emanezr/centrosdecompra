<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recepcion extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'numero', 'fecha', 'status', 'qqneto', 'kgneto', 'qqporpagar', 'kgporpagar',
        'promedioqq', 'caleta', 'totalcaleta', 'cantsacos', 'totaltara', 'observacion',
        'productor_id', 'sucursal_id', 'user_id'
    ];


    public function productos()
    {
        return $this->belongsToMany(Producto::class);
    }

    public function productor()
    {
        return $this->belongsTo(Productor::class);
    }
}
