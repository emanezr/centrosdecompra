<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasRoles;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'nombre', 'tlf', 'dir', 'nivel', 'sucursal_id', 'status', 'password',
    ];

    public $incrementing = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Relacion usuarios - Sucursal
     * 
     */
    
    public function sucursal()
    {
        return $this->belongsTo(Sucursal::class);
    }

    /**
     * Relacion usuarios - productos
     * 
     */

    public function productos()
    {
        return $this->hasMany(Producto::class);
    }

    public function scopeSucursal($query, $id)
    {
        if ($id) {
                
            return $query->where('sucursal_id', $id);

        }
    }

    public function scopeNivel($query, $nivel)
    {
        if ($nivel) {         
            
            return $query->where('nivel', $nivel);

        }
    }
}