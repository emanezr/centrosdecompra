<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    
    public function sucursales()
    {
        return $this->hasMany(Sucursal::class);
    }

    public function productores()
    {
        return $this->hasMany(Productor::class);
    }


}
