<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Caja extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'numero', 'status', 'descripcion', 'observacion', 'sucursal_id', 'bs'
    ];
    
    /**
     * Relacion usuarios - Sucursal
     * 
     */
    
    public function sucursal()
    {
        return $this->belongsTo(Sucursal::class);
    }


}
