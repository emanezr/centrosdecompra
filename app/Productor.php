<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Productor extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'nombre', 'status', 'tlf', 'dir', 'estado_id', 'sucursal_id'
    ];

    public $incrementing = false;
    
    
    public function estado()
    {
        return $this->belongsTo(Estado::class);
    }

    public function sucursal()
    {
        return $this->belongsTo(Sucursal::class);
    }

    public function recepciones()
    {
        return $this->hasMany(Recepcion::class);
    }

    public function scopeSucursal($query, $id)
    {
        if ($id) {
                
            return $query->where('sucursal_id', $id); 

        }
    }

    public function scopeEstado($query, $id)
    {
        if ($id) {
                
            return $query->where('estado_id', $id); 

        }
    }

}
