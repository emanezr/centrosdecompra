<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Beneficiario extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'nombre', 'tipocta', 'cta', 'correo', 'observacion', 'banco_id', 'status',
    ];

    /**
    * Relacion beneficiario - banco
    * 
    */
    public function banco() 
    {
        return $this->belongsTo(Banco::class);
    }

}
