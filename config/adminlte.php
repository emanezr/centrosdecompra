<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | The default title of your admin panel, this goes into the title tag
    | of your page. You can override it per page with the title section.
    | You can optionally also specify a title prefix and/or postfix.
    |
    */

    'title' => 'Centros de Compra',

    'title_prefix' => '',

    'title_postfix' => '',

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | This logo is displayed at the upper left corner of your admin panel.
    | You can use basic HTML here if you want. The logo has also a mini
    | variant, used for the mini side bar. Make it 3 letters or so
    |
    */

    'logo' => 'Centros de <b>Compra</b>',

    'logo_mini' => '<b>ITC</b>',

    /*
    |--------------------------------------------------------------------------
    | Skin Color
    |--------------------------------------------------------------------------
    |
    | Choose a skin color for your admin panel. The available skin colors:
    | blue, black, purple, yellow, red, and green. Each skin also has a
    | ligth variant: blue-light, purple-light, purple-light, etc.
    |
    */

    'skin' => 'black',

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Choose a layout for your admin panel. The available layout options:
    | null, 'boxed', 'fixed', 'top-nav'. null is the default, top-nav
    | removes the sidebar and places your menu in the top navbar
    |
    */

    'layout' => 'fixed',

    /*
    |--------------------------------------------------------------------------
    | Collapse Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we choose and option to be able to start with a collapsed side
    | bar. To adjust your sidebar layout simply set this  either true
    | this is compatible with layouts except top-nav layout option
    |
    */

    'collapse_sidebar' => false,

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Register here your dashboard, logout, login and register URLs. The
    | logout URL automatically sends a POST request in Laravel 5.3 or higher.
    | You can set the request to a GET or POST with logout_method.
    | Set register_url to null if you don't want a register link.
    |
    */

    'dashboard_url' => 'dashboard',

    'logout_url' => 'logout',

    'logout_method' => null,

    'login_url' => 'login',

    'register_url' => 'register',

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Specify your menu items to display in the left sidebar. Each menu item
    | should have a text and and a URL. You can also specify an icon from
    | Font Awesome. A string instead of an array represents a header in sidebar
    | layout. The 'can' is a filter on Laravel's built in Gate functionality.
    |
    */

    'menu' => [
        'PROCESOS DEL SISTEMA',
        [
            'text' => 'Recepciones',
            'url'  => 'recepciones',
            'icon' => 'truck',
        ],
        [
            'text' => 'Abonos',
            'url'  => '#',
            'icon' => 'line-chart',
        ],
        [
            'text' => 'Pagos',
            'url'  => '#',
            'icon' => 'money',
        ],
        [
            'text' => 'Ediciones',
            'url'  => '#',
            'icon' => 'pencil-square-o',
        ],
        'TABLAS GENERALES',
        [
            'text' => 'Usuarios',
            'url'  => '#',
            'icon' => 'users',
            //'can' => 'user-list',
            'roles' => [
                'Administrador', 'Auditor',
            ],
            'submenu' => [
                [
                    'text' => 'Listado de Usuarios',
                    'url'  => 'usuarios',
                ],
                [
                    'text'    => 'Crear Usuario',
                    'url'     => 'usuarios/nuevo',
                ],
            ],

        ],
        [
            'text' => 'Sucursales',
            'url'  => '#',
            'icon' => 'building',
            'roles' => [
                'Administrador', 'Auditor',
            ],
            'submenu' => [
                [
                    'text' => 'Listado de Sucursales',
                    'url'  => 'sucursales',
                ],
                [
                    'text'    => 'Crear Sucursal',
                    'url'     => 'sucursales/nuevo',
                ],
            ],

        ],
        [
            'text' => 'Bancos',
            'url'  => '#',
            'icon' => 'university',
            'roles' => [
                'Administrador', 'Auditor',
            ],
            'submenu' => [
                [
                    'text' => 'Listado de Bancos',
                    'url'  => 'bancos',
                ],
                [
                    'text'    => 'Crear Banco',
                    'url'     => 'bancos/nuevo',
                ],
            ],

        ],
        [
            'text' => 'Productos',
            'url'  => '#',
            'icon' => 'shopping-cart',
            'submenu' => [
                [
                    'text' => 'Listado de Productos',
                    'url'  => 'productos',
                ],
                [
                    'text'    => 'Crear Producto',
                    'url'     => 'productos/nuevo',
                    'roles' => [
                        'Administrador', 'Auditor',
                    ],
                ],
            ],

        ],
        [
            'text' => 'Productores',
            'url'  => '#',
            'icon' => 'users',
            'submenu' => [
                [
                    'text' => 'Listado de Productores',
                    'url'  => 'productores',
                ],
                [
                    'text'    => 'Crear Productor',
                    'url'     => 'productores/nuevo',
                ],
            ],

        ],
        [
            'text' => 'Beneficiarios',
            'url'  => '#',
            'icon' => 'users',
            'roles' => [
                'Administrador', 'Auditor',
            ],
            'submenu' => [
                [
                    'text' => 'Listado de Beneficiarios',
                    'url'  => 'beneficiarios',
                ],
                [
                    'text'    => 'Crear Beneficiario',
                    'url'     => 'beneficiarios/nuevo',
                ],
            ],

        ],
        [
            'text' => 'Caja Efectivo',
            'url'  => '#',
            'icon' => 'money',
            'roles' => [
                'Administrador', 'Auditor',
            ],
            'submenu' => [
                [
                    'text' => 'Listado de Cajas',
                    'url'  => 'cajas',
                ],
                [
                    'text'    => 'Crear Caja Efectivo',
                    'url'     => 'cajas/nuevo',
                ],
            ],

        ],
        [
            'text' => 'Variables',
            'url'  => 'variables',
            'icon' => 'cogs',
            'roles' => [
                'Administrador', 'Auditor',
            ],
        ],

    ],


    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Choose what filters you want to include for rendering the menu.
    | You can add your own filters to this array after you've created them.
    | You can comment out the GateFilter if you don't want to use Laravel's
    | built in Gate functionality
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SubmenuFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
        App\Menu\RoleMenuFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Choose which JavaScript plugins should be included. At this moment,
    | only DataTables is supported as a plugin. Set the value to true
    | to include the JavaScript file from a CDN via a script tag.
    |
    */

    'plugins' => [
        'datatables' => true,
        'select2'    => true,
        'chartjs'    => true,
    ],
];
