@extends('reportes.layout')

@section('content')
<header>
<div class="container">
    <div class="row">
        <div class="col-ms-6">
            <div class="pull-left">
                Inversiones Torrefacción del Café, C.A.
            </div>
        </div>
        <div class="col-ms-6">
            <div class="pull-right"> Reporte de Bancos</div>
        </div>
    </div>
</div>
</header>
<footer>
<div class="container">
    <div class="row">
        <div class="col-ms-6">
            <div class="pull-left">
                Inversiones Torrefacción del Café, C.A.
            </div>
        </div>
        <div class="col-ms-6">
            <div class="pull-right"> Reporte de Bancos</div>
        </div>
    </div>
</div>
</footer>
<main style="font-size: 12px;">
    <table class="table table-bordered table-striped table-condensed">
        <thead>
            <tr>
                <th>Codigo Banco</th>
                <th>Nombre del Banco</th>
            </tr>                            
        </thead>
        <tbody>
            @foreach($bancos as $banco)
                <tr>
                    <td>{{ $banco->id}} </td>
                    <td>{{ $banco->nombre }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</main>
@endsection