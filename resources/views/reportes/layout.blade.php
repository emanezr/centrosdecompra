<!doctype html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <style>
            @page {
                margin: 100px 30px;
            }

            header {
                position: fixed;
                top: -60px;
                left: 0px;
                right: 0px;
                height: 40px;

                /** Extra personal styles **/
                /*background-color: #03a9f4;*/
                color: #333;
                line-height: 35px;
                font-size: 12px;
            }

            footer {
                position: fixed; 
                bottom: -50px; 
                left: 0px; 
                right: 0px;
                height: 40px; 

                /** Extra personal styles **/
                color: #333;
                line-height: 35px;
                font-size: 12px;

            }
              </style>
    </head>
    <body>
        @yield('content')
    </body>
</html>