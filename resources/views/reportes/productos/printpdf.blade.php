@extends('reportes.layout')

@section('content')
<header>
<div class="container">
    <div class="row">
        <div class="col-ms-6">
            <div class="pull-left">
                Inversiones Torrefacción del Café, C.A.
            </div>
        </div>
        <div class="col-ms-6">
            <div class="pull-right"> Reporte de Productos</div>
        </div>
    </div>
</div>
</header>
<footer>
<div class="container">
    <div class="row">
        <div class="col-ms-6">
            <div class="pull-left">
                Inversiones Torrefacción del Café, C.A.
            </div>
        </div>
        <div class="col-ms-6">
            <div class="pull-right"> Reporte de Productos</div>
        </div>
    </div>
</div>
</footer>
<main style="font-size: 12px;">
    <table class="table table-bordered table-striped table-condensed">
        <thead>
            <tr>
                <th>Codigo</th>
                <th>Nombre</th>
                <th>Precio QQ</th>
                <th>Descripción</th>
                <th>Creado por</th>
            </tr>                            
        </thead>
        <tbody>
             @foreach($productos as $producto)
                <tr>
                    <td>{{ $producto->id}} </td>
                    <td>{{ $producto->nombre }}</td>
                    <td>{{ $producto->precioqq }}</td>
                    <td>{{ $producto->descripcion }}</td>
                    <td>{{ $producto->user->nombre }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</main>
@endsection