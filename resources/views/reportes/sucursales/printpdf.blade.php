@extends('reportes.layout')

@section('content')
<header>
<div class="container">
    <div class="row">
        <div class="col-ms-6">
            <div class="pull-left">
                Inversiones Torrefacción del Café, C.A.
            </div>
        </div>
        <div class="col-ms-6">
            <div class="pull-right"> Reporte de Sucursales</div>
        </div>
    </div>
</div>
</header>
<footer>
<div class="container">
    <div class="row">
        <div class="col-ms-6">
            <div class="pull-left">
                Inversiones Torrefacción del Café, C.A.
            </div>
        </div>
        <div class="col-ms-6">
            <div class="pull-right"> Reporte de Sucursales</div>
        </div>
    </div>
</div>
</footer>
<main style="font-size: 12px;">
    <table class="table table-bordered table-striped table-condensed">
        <thead>
            <tr>
                <th>Cod.</th>
                <th>Nombre</th>
                <th>Teléfono</th>
                <th>Estado</th>
                <th>Dirección</th>
            </tr>                            
        </thead>
        <tbody>
            @foreach($sucursales as $sucursal)
            <tr>
                <td>{{ $sucursal->id }}</td>
                <td>{{ $sucursal->nombre }}</td>
                <td>{{ $sucursal->tlf }}</td>
                <td>{{ $sucursal->estado->nombre }}</td>
                <td>{{ $sucursal->dir }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</main>
@endsection