@extends('reportes.layout')

@section('content')
<header>
<div class="container">
    <div class="row">
        <div class="col-ms-6">
            <div class="pull-left">
                Inversiones Torrefacción del Café, C.A.
            </div>
        </div>
        <div class="col-ms-6">
            <div class="pull-right"> Reporte de Usuarios</div>
        </div>
    </div>
</div>
</header>
<footer>
<div class="container">
    <div class="row">
        <div class="col-ms-6">
            <div class="pull-left">
                Inversiones Torrefacción del Café, C.A.
            </div>
        </div>
        <div class="col-ms-6">
            <div class="pull-right"> Reporte de Usuarios</div>
        </div>
    </div>
</div>
</footer>
<main style="font-size: 12px;">
    <table class="table table-bordered table-striped table-condensed">
        <thead>
            <tr>
                <th>Cedula</th>
                <th>Nombre</th>
                <th>Nivel</th>
                <th>Sucursal</th>
            </tr>                            
        </thead>
        <tbody>
            @foreach($users as $user)
            <tr>
                <td>{{ $user->id }}</td>
                <td>{{ $user->nombre }}</td>
                <td>{{ $user->nivel }}</td>
                <td>{{ $user->sucursal->nombre }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</main>
@endsection