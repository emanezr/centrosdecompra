@extends('adminlte::page')

@section('title', 'ITC - Centros de Compra | Listado de Usuarios')

@section('content_header')
@if(session()->has('status'))
    <div class="alert alert-{{ session()->get('alert') }} alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> {{ session()->get('status') }}</h4>
        {{ session()->get('mensaje') }}
    </div>
@endif

    <h1>Listado de Usuarios</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-8">
            <!-- Box -->
            <div class="box box-info">  
                <form action=" {{ route('reportes.users.index') }}" method="get">
                       
                    <div class="box-header with-border">
                        <h3 class="box-title">Parámetros de Busqueda</h3>
                    </div>             
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">                                
                                    <label for="sucursal_id" class="control-label">Filtrar por Sucursal</label>
                                    <select id="sucursal_id" name="sucursal_id" 
                                    class="form-control select2 select2-hidden-accessible" 
                                    style="width: 100%;" tabindex="-1" aria-hidden="true">
                                        <option value="" selected="selected">
                                            Todas las Sucursales
                                        </option>
                                    @foreach ($sucursales as $sucursal)
                                        <option value="{{ $sucursal->id }}"  @if($sucursal->id == $sucursal_id) selected="selected" @endif>
                                            {{ $sucursal->nombre }}
                                        </option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                    <div class="form-group">                                
                                        <label for="nivel" class="control-label">Filtrar por Permisos</label>
                                        <select id="nivel" name="nivel" 
                                        class="form-control select2 select2-hidden-accessible" 
                                        style="width: 100%;" tabindex="-1" aria-hidden="true">
                                            <option value="" selected="selected">
                                                Todos los Permisos
                                            </option>
                                            <option value="Usuario" @if($nivel == "Usuario") selected="selected" @endif>Usuario</option>
                                            <option value="Auditor" @if($nivel == "Auditor") selected="selected" @endif>Auditor</option>
                                            <option value="Administrador" @if($nivel == "Administrador") selected="selected" @endif>Administrador</option>
                                        </select>
                                    </div>
                                </div>

                        </div>
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-default btn-flat pull-right">
                                <i class="fa fa-search"></i>
                                &nbsp;Buscar
                            </button>
                        </div>
                    </div>
                </form>
            </div><!-- /.box -->

        </div><!-- /.col -->
        <div class="col-md-4">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Descargar reporte</h3>
                </div>
                <div class="box-body">
                    <table class="table no-border">
                        <tbody>
                            <tr>
                                <td><strong>Permisos de Usuarios: </strong></td>
                                @if($nivel)
                                    <td>{{ $nivel }}</td>
                                @else
                                    <td>Todos los Permisos</td>
                                @endif
                            </tr>
                            <tr>
                                <td><strong>Sucursal de Usuarios: </strong></td>
                                @if($sucursal_id)
                                    @foreach ($sucursales as $sucursal)
                                        @if($sucursal->id == $sucursal_id)
                                            <td>{{ $sucursal->nombre }}</td>
                                        @endif
                                    @endforeach
                                @else
                                    <td>Todas las Sucursales</td>
                                @endif
                            </tr>                           
                        </tbody>
                    </table>
                </div>
                <div class="box-footer">
                    <form action=" {{ route('reportes.users.printpdf') }}" method="get" target="_blank" >
                        <input type="hidden" id="nivelpdf" name="nivelpdf" 
                            value="{{ request()->has('nivel') ? $nivel : '' }}">
                        <input type="hidden" id="sucursalpdf" name="sucursalpdf" 
                            value="{{ request()->has('sucursal_id') ? $sucursal_id : '' }}">
                        <button type="submit" class="btn btn-block btn-danger">
                            <i class="fa fa-file-pdf-o fa-lg"></i> Descargar PDF
                        </button>                           
                    </form>
                </div>
            </div>
        </div>
    </div><!-- /.row -->

    <div class="row">
        <div class="col-md-12">
            <!-- Box -->
            <div class="box box-info">                    
                <div class="box-header with-border">
                    <h3 class="box-title">Resultados...</h3>
                    @if(request()->has('sucursal_id') or request()->has('nivel'))
                        {{ $users->appends($_GET)->links() }}
                    @else
                        {{ $users->links() }}
                    @endif
                </div>              
                <div class="box-body">
                    <table id="users-table" class="table table-hover table-bordered" role="grid">
                        <thead>
                            <tr>
                                <th>Cedula</th>
                                <th>Nombre</th>
                                <th>Nivel</th>
                                <th>Sucursal</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td>{{ $user->id}} </td>
                                <td>{{ $user->nombre }}</td>
                                <td>{{ $user->nivel }}</td>
                                <td>{{ $user->sucursal->nombre }}</td>
                            </tr>
                            @endforeach
                        </tbody>                           
                    </table>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    @if(request()->has('sucursal_id') or request()->has('nivel'))
                        {{ $users->appends($_GET)->links() }}
                    @else
                        {{ $users->links() }}
                    @endif
                </div>    
            </div><!-- /.box -->

        </div><!-- /.col -->

    </div><!-- /.row -->
@stop

@section('js')

<script>
    window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove(); 
        });
    }, 2500);
    $(function () {
        $('.select2').select2()
    });
</script>
@stop