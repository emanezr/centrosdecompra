@extends('adminlte::page')

@section('title', 'ITC - Centros de Compra | Listado de Usuarios')

@section('content_header')
@if(session()->has('status'))
    <div class="alert alert-{{ session()->get('alert') }} alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> {{ session()->get('status') }}</h4>
        {{ session()->get('mensaje') }}
    </div>
@endif

    <h1>Listado de Usuarios</h1>
@stop

@section('content')
    <div class="row">
            <div class="col-md-12">
                <!-- Box -->
                <div class="box box-info">
                    <div class="box-header with-border">
                    <h3 class="box-title">{{ $sucursal_id }}</h3>
                        </div>                
                    <div class="box-body">
                        <table id="users-table" class="table table-hover table-bordered dataTable" role="grid">
                                <thead>
                                    <tr>
                                        <th>Cedula</th>
                                        <th>Nombre</th>
                                        <th>Nivel</th>
                                        <th>Sucursal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($users as $user)
                                    <tr>
                                        <td>{{ $user->id}} </td>
                                        <td>{{ $user->nombre }}</td>
                                        <td>{{ $user->nivel }}</td>
                                        <td>{{ $user->sucursal->nombre }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                
                            </table>
                            <form action="" method="post">
                                
                            </form>
                        <a href="{{ route('reportes.users.listado') }}" class="btn" target="_blank">PDF</a>
                    </div><!-- /.box-body -->
                        
                </div><!-- /.box -->

            </div><!-- /.col -->

        </div><!-- /.row -->
@stop

@section('js')

<script>
    window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove(); 
        });
    }, 2500);
</script>
@stop