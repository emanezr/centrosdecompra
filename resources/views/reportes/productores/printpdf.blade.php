@extends('reportes.layout')

@section('content')
<header>
<div class="container">
    <div class="row">
        <div class="col-ms-6">
            <div class="pull-left">
                Inversiones Torrefacción del Café, C.A.
            </div>
        </div>
        <div class="col-ms-6">
            <div class="pull-right"> Reporte de Productores</div>
        </div>
    </div>
</div>
</header>
<footer>
<div class="container">
    <div class="row">
        <div class="col-ms-6">
            <div class="pull-left">
                Inversiones Torrefacción del Café, C.A.
            </div>
        </div>
        <div class="col-ms-6">
            <div class="pull-right"> Reporte de Productores</div>
        </div>
    </div>
</div>
</footer>
<main style="font-size: 12px;">
    <table class="table table-bordered table-striped table-condensed">
        <thead>
            <tr>
                <th>Cedula</th>
                <th>Nombre</th>
                <th>Teléfono</th>
                <th>Dirección</th>
                <th>Sucursal</th>
                <th>Estado</th>
            </tr>                            
        </thead>
        <tbody>
            @foreach($productores as $productor)
            <tr>
                <td>{{ $productor->id }}</td>
                <td>{{ $productor->nombre }}</td>
                <td>{{ $productor->tlf }}</td>
                <td>{{ $productor->dir }}</td>
                <td>{{ $productor->sucursal->nombre }}</td>
                <td>{{ $productor->estado->nombre }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</main>
@endsection