@extends('reportes.layout')

@section('content')
<header>
<div class="container">
    <div class="row">
        <div class="col-ms-6">
            <div class="pull-left">
                Inversiones Torrefacción del Café, C.A.
            </div>
        </div>
        <div class="col-ms-6">
            <div class="pull-right"> Reporte de Beneficiarios</div>
        </div>
    </div>
</div>
</header>
<footer>
<div class="container">
    <div class="row">
        <div class="col-ms-6">
            <div class="pull-left">
                Inversiones Torrefacción del Café, C.A.
            </div>
        </div>
        <div class="col-ms-6">
            <div class="pull-right">Reporte de Beneficiarios</div>
        </div>
    </div>
</div>
</footer>
<main style="font-size: 12px;">
    <table class="table table-bordered table-striped table-condensed">
        <thead>
            <tr>
                <th>RIF/Cedula</th>
                <th>Nombre</th>
                <th>T. Cuanta</th>
                <th>Nº de Cuenta</th>
                <th>Coreeo Elect.</th>
                <th>Banco</th>
                <th>Observación</th>

            </tr>
        </thead>
        <tbody>
            @foreach($beneficiarios as $beneficiario)
            <tr>
                <td>{{ $beneficiario->id}} </td>
                <td>{{ $beneficiario->nombre }}</td>
                <td>{{ $beneficiario->tipocta }}</td>
                <td>{{ $beneficiario->cta }}</td>
                <td>{{ $beneficiario->correo }}</td>
                <td>{{ $beneficiario->banco->nombre }}</td>
                <td>{{ $beneficiario->observacion }}</td>
            </tr>
            @endforeach
        </tbody>           
    </table>
</main>
@endsection