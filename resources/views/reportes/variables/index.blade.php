@extends('adminlte::page')

@section('title', 'ITC - Centros de Compra | Listado de Variables')

@section('content_header')
@if(session()->has('status'))
    <div class="alert alert-{{ session()->get('alert') }} alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> {{ session()->get('status') }}</h4>
        {{ session()->get('mensaje') }}
    </div>
@endif

    <h1>Listado de Variables</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Box -->
            <div class="box box-info">                    
                <div class="box-header">
                    <h3 class="box-title">Descargar reporte</h3>
                    <a href="{{ route('reportes.variables.printpdf') }}" target="_blank" type="submit" class="btn btn-danger pull-right">
                            <i class="fa fa-file-pdf-o fa-lg"></i> Descargar PDF
                    </a>  
                </div>              
                <div class="box-body">
                    <table id="variables-table" class="table table-hover table-bordered" role="grid">
                        <thead>
                            <tr>
                                <th>Kg QQ</th>
                                <th>Porcentaje</th>
                                <th>Saco Cocuiza</th>
                                <th>Saco Plastico</th>
                                <th>Caleta QQ</th>
                                <th>Caleta Kg</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach($variables as $variable)
                            <tr>
                                <td>{{ $variable->kgqq}} </td>
                                <td>{{ $variable->porcentaje }}</td>
                                <td>{{ $variable->sacococuiza }}</td>
                                <td>{{ $variable->sacoplastico }}</td>
                                <td>{{ $variable->caletaqq }}</td>
                                <td>{{ $variable->caletakg }}</td>
                            </tr>
                            @endforeach
                        </tbody>                           
                    </table>
                </div><!-- /.box-body -->  
            </div><!-- /.box -->

        </div><!-- /.col -->

    </div><!-- /.row -->
@stop

@section('js')

<script>
    window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove(); 
        });
    }, 2500);
    $(function () {
        $('.select2').select2()
    });
</script>
@stop