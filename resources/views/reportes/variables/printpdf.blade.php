@extends('reportes.layout')

@section('content')
<header>
<div class="container">
    <div class="row">
        <div class="col-ms-6">
            <div class="pull-left">
                Inversiones Torrefacción del Café, C.A.
            </div>
        </div>
        <div class="col-ms-6">
            <div class="pull-right"> Reporte de Variables</div>
        </div>
    </div>
</div>
</header>
<footer>
<div class="container">
    <div class="row">
        <div class="col-ms-6">
            <div class="pull-left">
                Inversiones Torrefacción del Café, C.A.
            </div>
        </div>
        <div class="col-ms-6">
            <div class="pull-right"> Reporte de Variables</div>
        </div>
    </div>
</div>
</footer>
<main style="font-size: 12px;">
    <table class="table table-bordered table-striped table-condensed">
        <thead>
            <tr>
                <th>Kg QQ</th>
                <th>Porcentaje</th>
                <th>Saco Cocuiza</th>
                <th>Saco Plastico</th>
                <th>Caleta QQ</th>
                <th>Caleta Kg</th>
            </tr>
        </thead>
        <tbody>
            @foreach($variables as $variable)
            <tr>
                <td>{{ $variable->kgqq}} </td>
                <td>{{ $variable->porcentaje }}</td>
                <td>{{ $variable->sacococuiza }}</td>
                <td>{{ $variable->sacoplastico }}</td>
                <td>{{ $variable->caletaqq }}</td>
                <td>{{ $variable->caletakg }}</td>
            </tr>
            @endforeach
        </tbody>            
    </table>
</main>
@endsection