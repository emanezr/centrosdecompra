@extends('adminlte::page')

@section('title', 'Centros de Compra | Crear Banco')

@section('content_header')
    <h1>Crear Banco</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <form action="{{ url('bancos') }}" class="form-horizontal" method="POST">
                    {{ csrf_field() }}

                    <div class="box-body">

                        <div class="form-group has-feedback {{ $errors->has('id') ? 'has-error' : '' }}" style="margin-top:20px">
                            <label for="id" class="col-sm-2 col-sm-offset-1 control-label">Cod. del Banco</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="id" name="id" max="4" required
                                placeholder="Ingresr cod. del banco" value="{{ old('id') }}">
                            @if ($errors->has('id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('id') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('nombre') ? 'has-error' : '' }}" style="margin-top:20px">
                            <label for="nombre" class="col-sm-2 col-sm-offset-1 control-label">Nombre del Banco</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="nombre" name="nombre" required
                                placeholder="Ingresar nombre del banco" value="{{ old('nombre') }}">
                            @if ($errors->has('nombre'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('nombre') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                    </div>
                    <div class="box-footer">
                            <div class="col-sm-7 col-sm-offset-3">
                                <button type="submit" class="btn btn-default btn-flat pull-right">
                                    <i class="fa fa-plus"></i>
                                    &nbsp;Crear Banco
                                </button>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('js')
<script>
    $(function () {
        $('.select2').select2()
    });
</script>
@stop