@extends('adminlte::page')

@section('title', 'Centros de Compra')

@section('content_header')
@if(session()->has('status'))
    <div class="alert alert-{{ session()->get('alert') }} alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> {{ session()->get('status') }}</h4>
        {{ session()->get('mensaje') }}
    </div>
@endif
    <h1>Dashboard</h1>
@stop

@section('content')

    <p>Welcome {{ Auth::user()->nombre }}, You are logged in!</p>
    <br>

@stop

@section('js')
<script>
    window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove(); 
        });
    }, 2500);

</script>
@stop