@extends('adminlte::page')

@section('title', 'Centros de Compra | Editar Productor')

@section('content_header')
<h1> {{ $productor->nombre }}</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                
                <form action="{{ url("/productores/{$productor->id}") }}" class="form-horizontal" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="box-body">

                        <div class="form-group has-feedback {{ $errors->has('id') ? 'has-error' : '' }}" style="margin-top:20px">
                            <label for="id" class="col-sm-2 col-sm-offset-1 control-label">Cod</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="id" name="id" 
                                placeholder="Ingresar cod del productor" value="{{ old('id', $productor->id) }}" disabled>
                            @if ($errors->has('id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('id') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('nombre') ? 'has-error' : '' }}" style="margin-top:20px">
                            <label for="nombre" class="col-sm-2 col-sm-offset-1 control-label">Nombre Completo</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="nombre" name="nombre" 
                                placeholder="Ingresar nombre completo" value="{{ old('nombre', $productor->nombre) }}">
                            @if ($errors->has('nombre'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('nombre') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('tlf') ? 'has-error' : '' }}" style="margin-top:20px">
                            <label for="tlf" class="col-sm-2 col-sm-offset-1 control-label">Número Telefónico</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="tlf" name="tlf" 
                                placeholder="Ingresar número telefónico" value="{{ old('tlf', $productor->tlf) }}">
                            @if ($errors->has('tlf'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('tlf') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('dir') ? 'has-error' : '' }}" style="margin-top:20px">
                            <label for="dir" class="col-sm-2 col-sm-offset-1 control-label">Dirección</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="dir" name="dir" 
                                placeholder="Ingresar dirección" value="{{ old('dir', $productor->dir) }}">
                            @if ($errors->has('dir'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('dir') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                        <div class="form-group @if($errors->has('estado_id')) {{ 'has-error' }} @endif">
                            <label for="estado_id" class="col-sm-2 col-sm-offset-1 control-label">Estado del Productor</label>
                            <div class="col-sm-7">
                                <select id="estado_id" name="estado_id" 
                                class="form-control select2 select2-hidden-accessible 
                                    has-feedback {{ $errors->has('estado_id') ? ' is-invalid' : '' }}" 
                                style="width: 100%;" tabindex="-1" aria-hidden="true">
                                @foreach ($estados as $estado)
                                    <option value="{{ $estado->id }}"  @if($estado->id == $productor->estado_id) selected="selected" @endif>
                                        {{ $estado->nombre }}
                                    </option>
                                @endforeach
                                </select>
                                @if($errors->has('estado_id'))
                                <span class="help-block">
                                    <i class="fa fa-times-circle-o"></i>
                                    {{ $errors->first('estado_id') }}
                                </span>
                                @endif
                            </div>
                        </div>

                    </div>
                    <div class="box-footer">
                            <div class="col-sm-7 col-sm-offset-3">
                                <a href="{{ route('productores.show', ['id' => $productor->id]) }}" class="btn btn-default btn-flat pull-left">
                                    <i class="fa fa-ban"></i>    
                                    &nbsp;Cancelar
                                </a>
                                <button type="submit" class="btn btn-default btn-flat pull-right">
                                    <i class="fa fa-refresh"></i>&nbsp;&nbsp;
                                    Actualizar productor
                                </button>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('js')
<script>
    $(function () {
        $('.select2').select2()
    });
</script>
@stop