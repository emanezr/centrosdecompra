<ul class="nav navbar-nav">
    <!-- Notifications: style can be found in dropdown.less -->
    <li class="dropdown notifications-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color:#ffffff; background:#333">
            REPORTES
        </a>
    </li>
</ul>

@hasanyrole('Administrador|Auditor')
<ul class="nav navbar-nav">
        <!-- Notifications: style can be found in dropdown.less -->
        <li class="dropdown notifications-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-table"></i> Tablas
        </a>
        <ul class="dropdown-menu">
            <li class="header"><strong>REPORTE GENERAL DE TABLAS </strong></li>
            <li>
            <!-- inner menu: contains the actual data -->
            <ul class="menu">
                <li>
                <a href="{{ url('reportes/tablas/users') }} ">
                    <i class="fa fa-fw fa-users"></i> Usuarios
                </a>
                </li>
                <li>
                <a href="{{ url('reportes/tablas/sucursales') }} ">
                    <i class="fa fa-fw fa-building"></i> Sucursales
                </a>
                </li>
                <li>
                <a href="{{ url('reportes/tablas/bancos') }} ">
                    <i class="fa fa-fw fa-university"></i> Bancos
                </a>
                </li>
                <li>
                <a href="{{ url('reportes/tablas/productos') }} ">
                    <i class="fa fa-fw fa-shopping-cart"></i> Productos
                </a>
                </li>
                <li>
                <a href="{{ url('reportes/tablas/productores') }}">
                    <i class="fa fa-fw fa-users"></i> Productores
                </a>
                </li>
                <li>
                <a href="{{ url('reportes/tablas/beneficiarios') }}">
                    <i class="fa fa-fw fa-users"></i> Beneficiarios
                </a>
                </li>
                <li>
                <a href="{{ url('reportes/tablas/variables') }}">
                    <i class="fa fa-fw fa-cogs"></i> Variables
                </a>
                </li>
            </ul>
            </li>
        </ul>
    </li>
</ul>
@endhasanyrole


<ul class="nav navbar-nav">
    <!-- Notifications: style can be found in dropdown.less -->
    <li class="dropdown notifications-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-area-chart"></i> Inventario
        </a>
    </li>
</ul>

<ul class="nav navbar-nav">
    <!-- Notifications: style can be found in dropdown.less -->
    <li class="dropdown notifications-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-exchange"></i> Movimientos
        </a>
    </li>
</ul>

<ul class="nav navbar-nav">
    <!-- Notifications: style can be found in dropdown.less -->
    <li class="dropdown notifications-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-coffee"></i> Café SP
        </a>
    </li>
</ul>

<ul class="nav navbar-nav">
    <!-- Notifications: style can be found in dropdown.less -->
    <li class="dropdown notifications-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa  fa-bar-chart"></i> Prestamos
        </a>
    </li>
</ul>

<ul class="nav navbar-nav">
    <!-- Notifications: style can be found in dropdown.less -->
    <li class="dropdown notifications-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-fw fa-money"></i> Cajas
        </a>
    </li>
</ul>

@hasanyrole('Administrador|Auditor')
<ul class="nav navbar-nav">
    <!-- Notifications: style can be found in dropdown.less -->
    <li class="dropdown notifications-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa  fa-pencil-square-o"></i> Ediciones
        </a>
    </li>
</ul>
@endhasanyrole