@extends('adminlte::page')

@section('title', 'ITC - Centros de Compra | Listado de Recepciones')

@section('content_header')
@if(session()->has('status'))
    <div class="alert alert-{{ session()->get('alert') }} alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> {{ session()->get('status') }}</h4>
        {{ session()->get('mensaje') }}
    </div>
@endif

    <h1>Listado de Recepciones</h1>
@stop

@section('content')
    <div class="row">
            <div class="col-md-12">
                <!-- Box -->
                <div class="box box-info">               
                    <div class="box-body">

                    </div><!-- /.box-body -->
                        
                </div><!-- /.box -->

            </div><!-- /.col -->

        </div><!-- /.row -->
@stop

@section('js')

<script>
    window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove(); 
        });
    }, 2500);
</script>
@stop