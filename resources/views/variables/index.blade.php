
@extends('adminlte::page')

@section('title', 'Centros de Compra | Variables')

@section('content_header')
@if(session()->has('status'))
    <div class="alert alert-{{ session()->get('alert') }} alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> {{ session()->get('status') }}</h4>
        {{ session()->get('mensaje') }}
    </div>
@endif

<h1> Variables del Sistema </h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="form-horizontal center-block">

                    <div class="box-body">

                        <div class="form-group" style="margin-top:20px">
                            <label for="kgqq" class="col-sm-2 col-sm-offset-1 control-label">Kg QQ</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="kgqq" name="kgqq" 
                                value="{{ $variable->kgqq }}" disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="porcentaje" class="col-sm-2 col-sm-offset-1 control-label">Porcentaje</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="porcentaje" name="porcentaje" 
                                value="{{ $variable->porcentaje }}" disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="sacococuiza" class="col-sm-2 col-sm-offset-1 control-label">Saco Cocuiza</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="sacococuiza" name="sacococuiza" 
                                value="{{ $variable->sacococuiza }}" disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="sacoplastico" class="col-sm-2 col-sm-offset-1 control-label">Saco Plastico</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="sacoplastico" name="sacoplastico" 
                                value="{{ $variable->sacoplastico }}" disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="caletaqq" class="col-sm-2 col-sm-offset-1 control-label">Caleta QQ</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="caletaqq" name="caletaqq" 
                                value="{{ $variable->caletaqq }}" disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="caletakg" class="col-sm-2 col-sm-offset-1 control-label">Caleta Kg</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="caletakg" name="caletakg" 
                                value="{{ $variable->caletakg }}" disabled>
                            </div>
                        </div>

                    </div>    
                    <div class="box-footer">
                        <div class="col-sm-7 col-sm-offset-3">
                            <a href="{{ route('variables.edit') }}" class="btn btn-default btn-flat pull-right">
                                <i class="fa fa-pencil"></i>
                                Editar
                            </a>
                        </div>
                    </div>
                
                </div>
            </div>
        </div>
    </div>

@stop

@section('js')
<script>
    window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove(); 
        });
    }, 2500);
</script>
@stop