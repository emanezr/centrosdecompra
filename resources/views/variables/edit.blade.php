@extends('adminlte::page')

@section('title', 'Centros de Compra | Editar Variables')

@section('content_header')
<h1> Editar Variables </h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                
                <form action="{{ url("/variables") }}" class="form-horizontal" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="box-body">

                        <div class="form-group has-feedback {{ $errors->has('id') ? 'has-error' : '' }}" style="margin-top:20px">
                            <label for="kgqq" class="col-sm-2 col-sm-offset-1 control-label">Kg QQ</label>
                            <div class="col-sm-7">
                            <input type="number" class="form-control" id="kgqq" name="kgqq" step="any"
                                placeholder="Ingresar Kg QQ" value="{{ old('kgqq', $variable->kgqq) }}">
                            @if ($errors->has('kgqq'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('kgqq') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('porcentaje') ? 'has-error' : '' }}" style="margin-top:20px">
                            <label for="porcentaje" class="col-sm-2 col-sm-offset-1 control-label">Porcentaje</label>
                            <div class="col-sm-7">
                            <input type="number" class="form-control" id="porcentaje" name="porcentaje" step="any"
                                placeholder="Ingresar porcentaje" value="{{ old('porcentaje', $variable->porcentaje) }}">
                            @if ($errors->has('porcentaje'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('porcentaje') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('sacococuiza') ? 'has-error' : '' }}" style="margin-top:20px">
                            <label for="sacococuiza" class="col-sm-2 col-sm-offset-1 control-label">Saco Cocuiza</label>
                            <div class="col-sm-7">
                            <input type="number" class="form-control" id="sacococuiza" name="sacococuiza" step="any"
                                placeholder="Ingresar saco cocuiza" value="{{ old('sacococuiza', $variable->sacococuiza) }}">
                            @if ($errors->has('sacococuiza'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('sacococuiza') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('sacoplastico') ? 'has-error' : '' }}" style="margin-top:20px">
                            <label for="sacoplastico" class="col-sm-2 col-sm-offset-1 control-label">Saco Plastico</label>
                            <div class="col-sm-7">
                            <input type="number" class="form-control" id="sacoplastico" name="sacoplastico" step="any"
                                placeholder="Ingresar saco plastico " value="{{ old('sacoplastico', $variable->sacoplastico) }}">
                            @if ($errors->has('sacoplastico'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('sacoplastico') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('caletaqq') ? 'has-error' : '' }}" style="margin-top:20px">
                            <label for="caletaqq" class="col-sm-2 col-sm-offset-1 control-label">Caleta QQ</label>
                            <div class="col-sm-7">
                            <input type="number" class="form-control" id="caletaqq" name="caletaqq" step="any"
                                placeholder="Ingresar caleta QQ" value="{{ old('caletaqq', $variable->caletaqq) }}">
                            @if ($errors->has('caletaqq'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('caletaqq') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('caletakg') ? 'has-error' : '' }}" style="margin-top:20px">
                            <label for="caletakg" class="col-sm-2 col-sm-offset-1 control-label">Caleta Kg</label>
                            <div class="col-sm-7">
                            <input type="number" class="form-control" id="caletakg" name="caletakg" step="any"
                                placeholder="Ingresar caleta kg" value="{{ old('caletakg', $variable->caletakg) }}">
                            @if ($errors->has('caletakg'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('caletakg') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                    </div>
                        
                    <div class="box-footer">
                            <div class="col-sm-7 col-sm-offset-3">
                                <a href="{{ route('variables.index') }}" class="btn btn-default btn-flat pull-left">
                                    <i class="fa fa-ban"></i>    
                                    &nbsp;Cancelar
                                </a>
                                <button type="submit" class="btn btn-default btn-flat pull-right">
                                    <i class="fa fa-refresh"></i>&nbsp;&nbsp;
                                    Actualizar Banco
                                </button>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('js')
<script>
    $(function () {
        $('.select2').select2()
    });
</script>
@stop