@extends('adminlte::page')

@section('title', 'Centros de Compra | Editar Usuario')

@section('content_header')
<h1> {{ $caja->numero }}</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                
                <form action="{{ url("/cajas/{$caja->id}") }}" class="form-horizontal" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="box-body">

                        <div class="form-group has-feedback {{ $errors->has('id') ? 'has-error' : '' }}" style="margin-top:20px">
                            <label for="id" class="col-sm-2 col-sm-offset-1 control-label">Cod</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="id" name="id" 
                                placeholder="Ingresr nuemro de cedúla" value="{{ old('id', $caja->id) }}" disabled>
                            @if ($errors->has('id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('id') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('numero') ? 'has-error' : '' }}" style="margin-top:20px">
                            <label for="numero" class="col-sm-2 col-sm-offset-1 control-label">Numero</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="numero" name="numero" 
                                placeholder="Ingresar numero" value="{{ old('numero', $caja->numero) }}">
                            @if ($errors->has('numero'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('numero') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('descripcion') ? 'has-error' : '' }}" style="margin-top:20px">
                            <label for="descripcion" class="col-sm-2 col-sm-offset-1 control-label">Descripcion</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="descripcion" name="descripcion" 
                                placeholder="Ingresar descripción" value="{{ old('descripcion', $caja->descripcion) }}">
                            @if ($errors->has('descripcion'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('descripcion') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('bs') ? 'has-error' : '' }}" style="margin-top:20px">
                            <label for="bs" class="col-sm-2 col-sm-offset-1 control-label">Bs</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="bs" name="bs" 
                                placeholder="Ingresar bsección" value="{{ old('bs', $caja->bs) }}">
                            @if ($errors->has('bs'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('bs') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('observacion') ? 'has-error' : '' }}" style="margin-top:20px">
                            <label for="observacion" class="col-sm-2 col-sm-offset-1 control-label">Observación</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="observacion" name="observacion" 
                                placeholder="Ingresar observación" value="{{ old('observacion', $caja->observacion) }}">
                            @if ($errors->has('observacion'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('observacion') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                        <div class="form-group @if($errors->has('sucursal_id')) {{ 'has-error' }} @endif">
                            <label for="sucursal_id" class="col-sm-2 col-sm-offset-1 control-label">Sucursal</label>
                            <div class="col-sm-7">
                                <select id="sucursal_id" name="sucursal_id" 
                                class="form-control select2 select2-hidden-accessible 
                                    has-feedback {{ $errors->has('sucursal_id') ? ' is-invalid' : '' }}" 
                                style="width: 100%;" tabindex="-1" aria-hidden="true">
                                @foreach ($sucursales as $sucursal)
                                    <option value="{{ $sucursal->id }}"  @if($sucursal->id == $caja->sucursal_id) selected="selected" @endif>
                                        {{ $sucursal->nombre }}
                                    </option>
                                @endforeach
                                </select>
                                @if($errors->has('sucursal_id'))
                                <span class="help-block">
                                    <i class="fa fa-times-circle-o"></i>
                                    {{ $errors->first('sucursal_id') }}
                                </span>
                                @endif
                            </div>
                        </div>

                    </div>
                    <div class="box-footer">
                            <div class="col-sm-7 col-sm-offset-3">
                                <a href="{{ route('cajas.show', ['id' => $caja->id]) }}" class="btn btn-default btn-flat pull-left">
                                    <i class="fa fa-ban"></i>    
                                    &nbsp;Cancelar
                                </a>
                                <button type="submit" class="btn btn-default btn-flat pull-right">
                                    <i class="fa fa-refresh"></i>&nbsp;&nbsp;
                                    Actualizar caja
                                </button>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('js')
<script>
    $(function () {
        $('.select2').select2()
    });
</script>
@stop