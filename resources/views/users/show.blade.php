
@extends('adminlte::page')

@section('title', 'Centros de Compra | {{ $user->nombre }}')

@section('content_header')
@if(session()->has('success'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Actualizado!</h4>
        {{ session()->get('success') }}
    </div>
@endif

<h1> {{ $user->nombre }} <small> Sucursal {{ $user->sucursal->nombre }}</small> </h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="form-horizontal center-block">

                    <div class="box-body">

                        <div class="form-group" style="margin-top:20px">
                            <label for="id" class="col-sm-2 col-sm-offset-1 control-label">Cedúla de Identidad</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="id" name="id" 
                                value="{{ $user->id }}" disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="nombre" class="col-sm-2 col-sm-offset-1 control-label">Nombre Completo</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="nombre" name="nombre" 
                                value="{{ $user->nombre }}" disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="tlf" class="col-sm-2 col-sm-offset-1 control-label">Número Telefónico</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="tlf" name="tlf" 
                                value="{{ $user->tlf }}" disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="dir" class="col-sm-2 col-sm-offset-1 control-label">Dirección</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="dir" name="dir" 
                                value="{{ $user->dir }}" disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="nivel" class="col-sm-2 col-sm-offset-1 control-label">Permisos del Usuario</label>
                            <div class="col-sm-7">
                                    <input type="nivel" class="form-control" id="nivel" name="nivel" 
                                    value="{{ $user->nivel }}" disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="sucursal_id" class="col-sm-2 col-sm-offset-1 control-label">Sucursal del Usuario</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="sucursal_id" name="sucursal_id" 
                                value="{{ $user->sucursal->nombre }}" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="col-sm-7 col-sm-offset-3">
                            <a href="{{ route('users.edit', ['id' => $user->id]) }}" class="btn btn-default btn-flat pull-left">
                                <i class="fa fa-pencil"></i>
                                Editar
                            </a>
                            <button type="button"  data-toggle="modal" class="btn btn-default btn-flat pull-right"
                            data-target="#modal-delete">
                                <i class="fa fa-trash"></i>
                                Eliminar
                            </button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-delete" style="display: none;">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                  <h4 class="modal-title">Eliminar Usuario</h4>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <p>La siguiente operacion eliminara al usuario <h4><strong>{{ $user->nombre }} </strong></h4></p>
                        <p class="text-red">¡Desea continuar con la eliminación.!</p>                  
                    </div>
                </div>
                <div class="modal-footer">
                    <form action="{{ route('users.destroy', $user) }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}                       
                        <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-flat btn-danger">Eliminar</button>
                    </form>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
@stop

@section('js')
<script>
    window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove(); 
        });
    }, 2500);
</script>
@stop