@extends('adminlte::page')

@section('title', 'Centros de Compra | Editar Usuario')

@section('content_header')
<h1> {{ $user->nombre }}</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                
                <form action="{{ url("/usuarios/{$user->id}") }}" class="form-horizontal" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="box-body">

                        <div class="form-group has-feedback {{ $errors->has('id') ? 'has-error' : '' }}" style="margin-top:20px">
                            <label for="id" class="col-sm-2 col-sm-offset-1 control-label">Cedúla de Identidad</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="id" name="id" 
                                placeholder="Ingresr nuemro de cedúla" value="{{ old('id', $user->id) }}" disabled>
                            @if ($errors->has('id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('id') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('nombre') ? 'has-error' : '' }}" style="margin-top:20px">
                            <label for="nombre" class="col-sm-2 col-sm-offset-1 control-label">Nombre Completo</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="nombre" name="nombre" 
                                placeholder="Ingresar nombre completo" value="{{ old('nombre', $user->nombre) }}">
                            @if ($errors->has('nombre'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('nombre') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('tlf') ? 'has-error' : '' }}" style="margin-top:20px">
                            <label for="tlf" class="col-sm-2 col-sm-offset-1 control-label">Número Telefónico</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="tlf" name="tlf" 
                                placeholder="Ingresar número telefónico" value="{{ old('tlf', $user->tlf) }}">
                            @if ($errors->has('tlf'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('tlf') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('dir') ? 'has-error' : '' }}" style="margin-top:20px">
                            <label for="dir" class="col-sm-2 col-sm-offset-1 control-label">Dirección</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="dir" name="dir" 
                                placeholder="Ingresar dirección" value="{{ old('dir', $user->dir) }}">
                            @if ($errors->has('dir'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('dir') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                        <div class="form-group @if($errors->has('nivel')) {{ 'has-error' }} @endif">
                            <label for="nivel" class="col-sm-2 col-sm-offset-1 control-label">Permisos del Usuario</label>
                            <div class="col-sm-7">
                                <select id="nivel" name="nivel" 
                                class="form-control select2 select2-hidden-accessible 
                                    has-feedback {{ $errors->has('nivel') ? ' is-invalid' : '' }}" 
                                style="width: 100%;" tabindex="-1" aria-hidden="true">
                                    <option value="Usuario" @if($user->nivel == "Usuario") selected="selected" @endif>Usuario</option>
                                    <option value="Auditor" @if($user->nivel == "Auditor") selected="selected" @endif>Auditor</option>
                                    <option value="Administrador" @if($user->nivel == "Administrador") selected="selected" @endif>Administrador</option>
                                </select>
                                @if($errors->has('nivel'))
                                <span class="help-block">
                                    <i class="fa fa-times-circle-o"></i>
                                    {{ $errors->first('nivel') }}
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group @if($errors->has('sucursal_id')) {{ 'has-error' }} @endif">
                            <label for="sucursal_id" class="col-sm-2 col-sm-offset-1 control-label">Sucursal del Usuario</label>
                            <div class="col-sm-7">
                                <select id="sucursal_id" name="sucursal_id" 
                                class="form-control select2 select2-hidden-accessible 
                                    has-feedback {{ $errors->has('sucursal_id') ? ' is-invalid' : '' }}" 
                                style="width: 100%;" tabindex="-1" aria-hidden="true">
                                @foreach ($sucursales as $sucursal)
                                    <option value="{{ $sucursal->id }}"  @if($sucursal->id == $user->sucursal_id) selected="selected" @endif>
                                        {{ $sucursal->nombre }}
                                    </option>
                                @endforeach
                                </select>
                                @if($errors->has('sucursal_id'))
                                <span class="help-block">
                                    <i class="fa fa-times-circle-o"></i>
                                    {{ $errors->first('sucursal_id') }}
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                            <label for="password" class="col-sm-2 col-sm-offset-1 control-label">Password</label>
                            <div class="col-sm-7">
                            <input type="password" class="form-control" id="password" name="password" 
                                placeholder="Ingresar password">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                            <label for="password_confirmation" class="col-sm-2 col-sm-offset-1 control-label">Confirmar Password</label>
                            <div class="col-sm-7">
                            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" 
                                placeholder="Confirmar password">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                    </div>
                    <div class="box-footer">
                            <div class="col-sm-7 col-sm-offset-3">
                                <a href="{{ route('users.show', ['id' => $user->id]) }}" class="btn btn-default btn-flat pull-left">
                                    <i class="fa fa-ban"></i>    
                                    &nbsp;Cancelar
                                </a>
                                <button type="submit" class="btn btn-default btn-flat pull-right">
                                    <i class="fa fa-refresh"></i>&nbsp;&nbsp;
                                    Actualizar usuario
                                </button>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('js')
<script>
    $(function () {
        $('.select2').select2()
    });
</script>
@stop