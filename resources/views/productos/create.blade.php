@extends('adminlte::page')

@section('title', 'Centros de Compra | Crear Producto')

@section('content_header')
    <h1>Crear Producto</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <form action="{{ url('productos') }}" class="form-horizontal" method="POST">
                    {{ csrf_field() }}

                    <div class="box-body">

                        <div class="form-group has-feedback {{ $errors->has('id') ? 'has-error' : '' }}" style="margin-top:20px">
                            <label for="id" class="col-sm-2 col-sm-offset-1 control-label">Cod. Producto</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="id" name="id" 
                                placeholder="Ingresr cod. producto" value="{{ old('id') }}">
                            @if ($errors->has('id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('id') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('nombre') ? 'has-error' : '' }}" style="margin-top:20px">
                            <label for="nombre" class="col-sm-2 col-sm-offset-1 control-label">Nombre del producto</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="nombre" name="nombre" 
                                placeholder="Ingresar nombre del producto" value="{{ old('nombre') }}">
                            @if ($errors->has('nombre'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('nombre') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('precioqq') ? 'has-error' : '' }}" style="margin-top:20px">
                            <label for="precioqq" class="col-sm-2 col-sm-offset-1 control-label">Precio del QQ</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="precioqq" name="precioqq" 
                                placeholder="Ingresar precio del QQ" value="{{ old('precioqq') }}">
                            @if ($errors->has('precioqq'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('precioqq') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('descripcion') ? 'has-error' : '' }}" style="margin-top:20px">
                            <label for="descripcion" class="col-sm-2 col-sm-offset-1 control-label">Descripción</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="descripcion" name="descripcion" 
                                placeholder="Ingresar descripción" value="{{ old('descripcion') }}">
                            @if ($errors->has('descripcion'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('descripcion') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                    </div>
                    <div class="box-footer">
                            <div class="col-sm-7 col-sm-offset-3">
                                <button type="submit" class="btn btn-default btn-flat pull-right">
                                    <i class="fa fa-plus"></i>
                                    &nbsp;Crear producto
                                </button>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('js')
<script>
    $(function () {
        $('.select2').select2()
    });
</script>
@stop