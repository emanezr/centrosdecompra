@extends('adminlte::page')

@section('title', 'Centros de Compra | Editar Producto')

@section('content_header')
<h1> {{ $producto->nombre }}</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                
                <form action="{{ url("/productos/{$producto->id}") }}" class="form-horizontal" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="box-body">

                        <div class="form-group has-feedback {{ $errors->has('id') ? 'has-error' : '' }}" style="margin-top:20px">
                            <label for="id" class="col-sm-2 col-sm-offset-1 control-label">Cod. Producto</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="id" name="id" 
                                placeholder="Ingresr cod. producto" value="{{ old('id', $producto->id) }}" disabled>
                            @if ($errors->has('id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('id') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('nombre') ? 'has-error' : '' }}" style="margin-top:20px">
                            <label for="nombre" class="col-sm-2 col-sm-offset-1 control-label">Nombre del producto</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="nombre" name="nombre" 
                                placeholder="Ingresar nombre del producto" value="{{ old('nombre', $producto->nombre) }}">
                            @if ($errors->has('nombre'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('nombre') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('precioqq') ? 'has-error' : '' }}" style="margin-top:20px">
                            <label for="precioqq" class="col-sm-2 col-sm-offset-1 control-label">Precio del QQ</label>
                            <div class="col-sm-7">
                            <input type="number" class="form-control" id="precioqq" name="precioqq" step="any"
                                placeholder="Ingresar precio del QQ" value="{{ old('precioqq', $producto->precioqq) }}">
                            @if ($errors->has('precioqq'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('precioqq') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('descripcion') ? 'has-error' : '' }}" style="margin-top:20px">
                            <label for="descripcion" class="col-sm-2 col-sm-offset-1 control-label">Descripción</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="descripcion" name="descripcion" 
                                placeholder="Ingresar descripción" value="{{ old('descripcion', $producto->descripcion) }}">
                            @if ($errors->has('descripcion'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('descripcion') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                    </div>
                    <div class="box-footer">
                            <div class="col-sm-7 col-sm-offset-3">
                                <a href="{{ route('productos.show', ['id' => $producto->id]) }}" class="btn btn-default btn-flat pull-left">
                                    <i class="fa fa-ban"></i>    
                                    &nbsp;Cancelar
                                </a>
                                <button type="submit" class="btn btn-default btn-flat pull-right">
                                    <i class="fa fa-refresh"></i>&nbsp;&nbsp;
                                    Actualizar producto
                                </button>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('js')
<script>
    $(function () {
        $('.select2').select2()
    });
</script>
@stop