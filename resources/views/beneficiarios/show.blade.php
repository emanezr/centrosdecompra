
@extends('adminlte::page')

@section('title', 'Centros de Compra | {{ $beneficiario->nombre }}')

@section('content_header')
@if(session()->has('success'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Actualizado!</h4>
        {{ session()->get('success') }}
    </div>
@endif

<h1> {{ $beneficiario->nombre }} </h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="form-horizontal center-block">

                    <div class="box-body">

                        <div class="form-group" style="margin-top:20px">
                            <label for="id" class="col-sm-2 col-sm-offset-1 control-label">RIF / Cedúla de Identidad</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="id" name="id" 
                                value="{{ $beneficiario->id }}" disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="nombre" class="col-sm-2 col-sm-offset-1 control-label">Nombre Completo</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="nombre" name="nombre" 
                                value="{{ $beneficiario->nombre }}" disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="tipocta" class="col-sm-2 col-sm-offset-1 control-label">Tipo de Cuenta</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="tipocta" name="tipocta" 
                                value="{{ $beneficiario->tipocta }}" disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="cta" class="col-sm-2 col-sm-offset-1 control-label">Nº de Cuenta</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="cta" name="cta" 
                                value="{{ $beneficiario->cta }}" disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="correo" class="col-sm-2 col-sm-offset-1 control-label">Correo Electronico</label>
                            <div class="col-sm-7">
                                    <input type="text" class="form-control" id="correo" name="correo" 
                                    value="{{ $beneficiario->correo }}" disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="banco_id" class="col-sm-2 col-sm-offset-1 control-label">Correo Electronico</label>
                            <div class="col-sm-7">
                                    <input type="text" class="form-control" id="banco_id" name="banco_id" 
                                    value="{{ $beneficiario->banco->nombre }}" disabled>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="observacion" class="col-sm-2 col-sm-offset-1 control-label">Observacion</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="observacion" name="observacion" 
                                value="{{ $beneficiario->observacion }}" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="col-sm-7 col-sm-offset-3">
                            <a href="{{ route('beneficiarios.edit', ['id' => $beneficiario->id]) }}" class="btn btn-default btn-flat pull-left">
                                <i class="fa fa-pencil"></i>
                                Editar
                            </a>
                            <button type="button"  data-toggle="modal" class="btn btn-default btn-flat pull-right"
                            data-target="#modal-delete">
                                <i class="fa fa-trash"></i>
                                Eliminar
                            </button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-delete" style="display: none;">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                  <h4 class="modal-title">Eliminar Beneficiario</h4>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <p>La siguiente operacion eliminara al beneficiario <h4><strong>{{ $beneficiario->nombre }} </strong></h4></p>
                        <p class="text-red">¡Desea continuar con la eliminación.!</p>                  
                    </div>
                </div>
                <div class="modal-footer">
                    <form action="{{ route('beneficiarios.destroy', $beneficiario) }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}                       
                        <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-flat btn-danger">Eliminar</button>
                    </form>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
@stop

@section('js')
<script>
    window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove(); 
        });
    }, 2500);
</script>
@stop