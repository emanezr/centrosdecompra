@extends('adminlte::page')

@section('title', 'Centros de Compra | Crear Beneficiarios')

@section('content_header')
    <h1>Crear Beneficiario</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <form action="{{ url('beneficiarios') }}" class="form-horizontal" method="POST">
                    {{ csrf_field() }}

                    <div class="box-body">

                        <div class="form-group has-feedback {{ $errors->has('id') ? 'has-error' : '' }}" style="margin-top:20px">
                            <label for="id" class="col-sm-2 col-sm-offset-1 control-label">RIF / Cedúla de Identidad</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="id" name="id" 
                                placeholder="Ingresr nuemro de cedúla o RIF" value="{{ old('id') }}">
                            @if ($errors->has('id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('id') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('nombre') ? 'has-error' : '' }}" style="margin-top:20px">
                            <label for="nombre" class="col-sm-2 col-sm-offset-1 control-label">Nombre Completo</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="nombre" name="nombre" 
                                placeholder="Ingresar nombre completo" value="{{ old('nombre') }}">
                            @if ($errors->has('nombre'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('nombre') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                        <div class="form-group @if($errors->has('tipocta')) {{ 'has-error' }} @endif">
                            <label for="tipocta" class="col-sm-2 col-sm-offset-1 control-label">Tipo de Cuenta</label>
                            <div class="col-sm-7">
                                <select id="tipocta" name="tipocta" 
                                class="form-control select2 select2-hidden-accessible 
                                    has-feedback {{ $errors->has('tipocta') ? ' is-invalid' : '' }}" 
                                style="width: 100%;" tabindex="-1" aria-hidden="true">
                                    <option value="Corriente" selected="selected">Corriente</option>
                                    <option value="Ahorro">Ahorro</option>
                                </select>
                                @if($errors->has('tipocta'))
                                <span class="help-block">
                                    <i class="fa fa-times-circle-o"></i>
                                    {{ $errors->first('tipocta') }}
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('cta') ? 'has-error' : '' }}" style="margin-top:20px">
                            <label for="cta" class="col-sm-2 col-sm-offset-1 control-label">Nº de Cuenta</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="cta" name="cta" 
                                placeholder="Ingresar Nº de cuenta" value="{{ old('cta') }}">
                            @if ($errors->has('cta'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('cta') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('correo') ? 'has-error' : '' }}" style="margin-top:20px">
                            <label for="correo" class="col-sm-2 col-sm-offset-1 control-label">Correo Electronico</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="correo" name="correo" 
                                placeholder="Ingresar correo electronico" value="{{ old('correo') }}">
                            @if ($errors->has('correo'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('correo') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>


                        <div class="form-group @if($errors->has('banco_id')) {{ 'has-error' }} @endif">
                            <label for="banco_id" class="col-sm-2 col-sm-offset-1 control-label">Banco</label>
                            <div class="col-sm-7">
                                <select id="banco_id" name="banco_id" 
                                class="form-control select2 select2-hidden-accessible 
                                    has-feedback {{ $errors->has('banco_id') ? ' is-invalid' : '' }}" 
                                style="width: 100%;" tabindex="-1" aria-hidden="true">
                                @foreach ($bancos as $banco)
                                    <option value="{{ $banco->id }}">
                                        {{ $banco->nombre }}
                                    </option>
                                @endforeach
                                </select>
                                @if($errors->has('banco_id'))
                                <span class="help-block">
                                    <i class="fa fa-times-circle-o"></i>
                                    {{ $errors->first('banco_id') }}
                                </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group has-feedback {{ $errors->has('observacion') ? 'has-error' : '' }}" style="margin-top:20px">
                            <label for="observacion" class="col-sm-2 col-sm-offset-1 control-label">Observación</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control" id="observacion" name="observacion" 
                                placeholder="Ingresar observación" value="{{ old('observacion') }}">
                            @if ($errors->has('observacion'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('observacion') }}</strong>
                                </span>
                            @endif
                            </div>
                        </div>

                    </div>
                    <div class="box-footer">
                            <div class="col-sm-7 col-sm-offset-3">
                                <button type="submit" class="btn btn-default btn-flat pull-right">
                                    <i class="fa fa-plus"></i>
                                    &nbsp;Crear Beneficiario
                                </button>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('js')
<script>
    $(function () {
        $('.select2').select2()
    });
</script>
@stop