@extends('adminlte::page')

@section('title', 'ITC - Centros de Compra | Listado de Beneficiarios')

@section('content_header')
@if(session()->has('status'))
    <div class="alert alert-{{ session()->get('alert') }} alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> {{ session()->get('status') }}</h4>
        {{ session()->get('mensaje') }}
    </div>
@endif

    <h1>Listado de Beneficiarios</h1>
@stop

@section('content')
    <div class="row">
            <div class="col-md-12">
                <!-- Box -->
                <div class="box box-info">               
                    <div class="box-body">
                        <table id="beneficiarios-table" class="table table-hover table-bordered dataTable" role="grid">
                            <thead>
                                <tr>
                                    <th>RIF / Cedula</th>
                                    <th>Nombre</th>
                                    <th>Tipo de cuenta</th>
                                    <th>Nº de Cuenta</th>
                                    <th>Correo Electronico</th>
                                    <th>Banco</th>
                                    <th>Observación</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>RIF / Cedula</th>
                                    <th>Nombre</th>
                                    <th>Tipo de cuenta</th>
                                    <th>Nº de Cuenta</th>
                                    <th>Correo Electronico</th>
                                    <th>Banco</th>
                                    <th>Observación</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div><!-- /.box-body -->
                        
                </div><!-- /.box -->

            </div><!-- /.col -->

        </div><!-- /.row -->
@stop

@section('js')
<script>
    $(function () {
        $('#beneficiarios-table').DataTable({
            language: {
                url: 'https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
            },
            autoWidth   : false,
            columnDefs: [
                { width: 100, targets: 6 }
            ],
            serverSide: true,
            processing: true,
            ajax: '/beneficiarios/data/datatables',
            columns: [
                {data: 'id'},
                {data: 'nombre'},
                {data: 'tipocta'},
                {data: 'cta'},
                {data: 'correo'},
                {data: 'banco'},
                {data: 'observacion'},
                {data: 'acciones', name: 'actions', orderable: false, searchable: false}
            ]
        });
    });
</script>

<script>
    window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove(); 
        });
    }, 2500);
</script>
@stop