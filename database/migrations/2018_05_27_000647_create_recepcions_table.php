<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecepcionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recepcions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('numero')->unsigned();
            $table->dateTime('fecha');
            $table->string('status');
            $table->decimal('qqneto', 12, 2);
            $table->decimal('kgneto', 12, 2);
            $table->decimal('qqporpagar', 12, 2);
            $table->decimal('kgporpagar', 12, 2);
            $table->decimal('promedioqq', 12, 2);
            $table->integer('caleta');
            $table->decimal('totalcaleta', 12, 2);
            $table->integer('cantsacos');
            $table->decimal('totaltara', 12, 2);
            $table->text('observacion');
            $table->string('productor_id');
            $table->string('sucursal_id');
            $table->string('user_id');
            $table->foreign('productor_id')->references('id')->on('productors');
            $table->foreign('sucursal_id')->references('id')->on('sucursals');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recepcions');
    }
}
