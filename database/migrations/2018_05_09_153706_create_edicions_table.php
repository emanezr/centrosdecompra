<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEdicionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('edicions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('proceso');
            $table->integer('registro');
            $table->string('campo');
            $table->string('observacion');
            $table->string('useradmin');
            $table->string('usersucursal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('edicions');
    }
}
