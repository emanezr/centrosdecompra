<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBeneficiariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beneficiarios', function (Blueprint $table) {
            $table->string('id')->unique()->primary();
            $table->string('nombre');
            $table->string('tipocta');
            $table->string('cta');
            $table->string('correo');
            $table->string('observacion');
            $table->string('status')->default('activo');
            $table->string('banco_id', 4);
            $table->foreign('banco_id')->references('id')->on('bancos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    
    public function down()
    {
        Schema::dropIfExists('beneficiarios');
    }
}
