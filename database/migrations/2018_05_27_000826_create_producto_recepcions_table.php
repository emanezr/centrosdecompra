<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductoRecepcionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producto_recepcions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('producto_id');
            $table->integer('recepcion_id')->unsigned();
            $table->string('status');
            $table->decimal('qqneto', 12, 2);
            $table->decimal('kgneto', 12, 2);
            $table->decimal('qqporpagar', 12, 2);
            $table->decimal('kgporpagar', 12, 2);
            $table->decimal('promedioqq', 12, 2);
            $table->decimal('tara', 12, 2);
            $table->decimal('totaltara', 12, 2);
            $table->integer('cantsacos');
            $table->decimal('kgpaleta', 12, 2);
            $table->foreign('producto_id')->references('id')->on('productos');
            $table->foreign('recepcion_id')->references('id')->on('recepcions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producto_recepcions');
    }
}
