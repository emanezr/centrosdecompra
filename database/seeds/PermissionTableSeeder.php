<?php

use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Database\Seeder;
use App\User;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions_admin = [
            //CRUD USER
            'user-list',
            'user-create',
            'user-edit',
            'user-delete',
            'user-report',
            //CRUD Sucursal
            'sucursal-list',
            'sucursal-create',
            'sucursal-edit',
            'sucursal-delete',
            'sucursal-report',
            //CRUD Variables
            'variable-list',
            'variable-create',
            'variable-edit',
            'variable-delete',
            'variable-report',
            //CRUD Productos
            'producto-list',
            'producto-create',
            'producto-edit',
            'producto-delete',
            'producto-report',
            //CRUD Productores
            'productor-list',
            'productor-create',
            'productor-edit',
            'productor-delete',
            'productor-report',
            //CRUD Bancos
            'banco-list',
            'banco-create',
            'banco-edit',
            'banco-delete',
            'banco-report',
            //CRUD Caja
            'caja-list',
            'caja-create',
            'caja-edit',
            'caja-delete',
            'caja-report',
            //CRUD Beneficiarios
            'beneficiario-list',
            'beneficiario-create',
            'beneficiario-edit',
            'beneficiario-delete',
            'beneficiario-report',
        ];

        $permissions_audit = [
            //CRUD USER
            'user-list',
            'user-report',
            //CRUD Sucursal
            'sucursal-list',
            'sucursal-report',
            //CRUD Variables
            'variable-list',
            'variable-report',
            //CRUD Productos
            'producto-list',
            'producto-report',
            //CRUD Productores
            'productor-list',
            'productor-report',
            //CRUD Bancos
            'banco-list',
            'banco-report',
            //CRUD Caja
            'caja-list',
            //CRUD Beneficiarios
            'beneficiario-list',
            'beneficiario-report',
        ];
        
        $permissions_user = [
            //CRUD USER
            'user-list',
            //CRUD Variables
            'variable-list',
            //CRUD Productos
            'producto-list',
            //CRUD Productores
            'productor-list',
            'productor-create',
            'productor-edit',
            'productor-delete',
        ];

        foreach ($permissions_admin as $permission_admin) {
            Permission::create(['name' => $permission_admin]);
        }
        
        $admin =  Role::create(['name' => 'Administrador']);
        $auditor = Role::create(['name' => 'Auditor']);
        $usuario = Role::create(['name' => 'Usuario']);

        $admin->syncPermissions($permissions_admin);
        $auditor->syncPermissions($permissions_audit);
        $usuario->syncPermissions($permissions_user);

        $users = User::select()->get();
    
        foreach ($users as $user) {
            if ($user->nivel == 'Administrador') {
                $user->assignRole('Administrador');
            }
            elseif ($user->nivel == 'Auditor') {
                $user->assignRole('Auditor');
            }
            elseif ($user->nivel == 'Usuario') {
                $user->assignRole('Usuario');
            }
        }
        // $user = User::find('17341248');
        // $user->assignRole('Administrador');

    }
}
