<?php

use Illuminate\Database\Seeder;
use App\Producto;

class ProductoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Producto::create([
            'id' => 'AA1122',
            'nombre' => 'Granos Arábica Verde',
            'status' => 'activo',
            'precioqq' => 2500000,
            'descripcion' => 'Café Arábica en granos verdes',
            'user_id' => '17341248',
        ]);
        Producto::create([
            'id' => 'AA2222',
            'nombre' => 'Café Arábica Lavado',
            'status' => 'activo',
            'precioqq' => 5050000,
            'descripcion' => 'Granos de Café Arábica Lavado',
            'user_id' => '17341248',
        ]);
        Producto::create([
            'id' => 'BB1122',
            'nombre' => 'Café Robusta no Lavado',
            'status' => 'activo',
            'precioqq' => 4050000,
            'descripcion' => 'Granos de Café Robusta no Lavado',
            'user_id' => '15213792',
        ]);
        Producto::create([
            'id' => 'BB2222',
            'nombre' => 'Café Robusta Lavado',
            'status' => 'activo',
            'precioqq' => 6200000,
            'descripcion' => 'Granos de Café Robusta Lavado',
            'user_id' => '15213792',
        ]);
    }
}
