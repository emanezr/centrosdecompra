<?php

use App\Sucursal;
use Illuminate\Database\Seeder;

class SucursalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sucursal::create([
            'id' => 'gene',
            'nombre' => 'General',
            'tlf' => '123456789',
            'dir' => 'Araure',
            'estado_id' => 17
        ]);
        Sucursal::create([
            'id' => 'arau',
            'nombre' => 'Araure',
            'tlf' => '123456789',
            'dir' => 'Araure',
            'estado_id' => 17
        ]);
        Sucursal::create([
            'id' => 'ospi',
            'nombre' => 'Ospino',
            'tlf' => '123456789',
            'dir' => 'Ospino',
            'estado_id' => 17
        ]);
        Sucursal::create([
            'id' => 'sana',
            'nombre' => 'Sanare',
            'tlf' => '123456789',
            'dir' => 'Sanare',
            'estado_id' => 12
        ]);
        Sucursal::create([
            'id' => 'bisc',
            'nombre' => 'Biscucuy',
            'tlf' => '123456789',
            'dir' => 'Biscucuy',
            'estado_id' => 17
        ]);
    }
}
