<?php

use Illuminate\Database\Seeder;
use App\Variable;

class VariablesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Variable::create([
            'kgqq' => 46, 
            'porcentaje' => 30, 
            'sacococuiza' => 1.5, 
            'sacoplastico' => 1, 
            'caletaqq' => 2000,
            'caletakg' => 44,  
        ]);
    }
}
