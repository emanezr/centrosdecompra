<?php

use Illuminate\Database\Seeder;
use App\Caja;

class CajasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Caja::create([
            'numero' => 123,
            'descripcion' => 'Caja 123',
            'bs' => '1000000000',
            'sucursal_id' => 'arau',
            'observacion' => 'Cajaaa 123',
        ]);

        Caja::create([
            'numero' => 456,
            'descripcion' => 'Caja 456',
            'bs' => '200000000',
            'sucursal_id' => 'ospi',
            'observacion' => 'Cajaaaa 456',
        ]);

        Caja::create([
            'numero' => 789,
            'descripcion' => 'Caja 789',
            'bs' => '100000000',
            'sucursal_id' => 'sana',
            'observacion' => 'Cajaaaa 789',
        ]);

    }
}
