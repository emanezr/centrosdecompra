<?php

use Illuminate\Database\Seeder;
use App\Productor;

class ProductorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Productor::create([
            'id' => '11111111',
            'nombre' => 'Jose Antonio Paez',
            'tlf' => '02551555555',
            'dir' => 'Guanare',
            'estado_id' => 17,
            'sucursal_id' => 'bisc'
        ]);

        Productor::create([
            'id' => '22222222',
            'nombre' => 'Francisco de Miranda',
            'tlf' => '02552555555',
            'dir' => 'Araure',
            'estado_id' => 17,
            'sucursal_id' => 'arau'
        ]);
        
        Productor::create([
            'id' => '44444444',
            'nombre' => 'Andrés Bello',
            'tlf' => '02553555555',
            'dir' => 'Ospino',
            'estado_id' => 17,
            'sucursal_id' => 'ospi'
        ]);

        Productor::create([
            'id' => '33333333',
            'nombre' => 'Antonio José de Sucre',
            'tlf' => '02554555555',
            'dir' => 'Sanare',
            'estado_id' => 12,
            'sucursal_id' => 'sana'
        ]);

        Productor::create([
            'id' => '55555555',
            'nombre' => 'Simón Rodríguez',
            'tlf' => '02555555555',
            'dir' => 'Acarigua',
            'estado_id' => 17,
            'sucursal_id' => 'arau'
        ]);
    }
}
