<?php

use Illuminate\Database\Seeder;
use App\Banco;

class BancoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get("database/data/bancos.json");
        $data = json_decode($json);

        foreach ($data as $obj) {
            
            Banco::create(
                [
                'nombre' => $obj->banco,
                'id' => $obj->codigo,
                ]
            );

        }
    }
}
