<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(EstadosTable::class);
        $this->call(SucursalTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(ProductorTableSeeder::class);
        $this->call(ProductoTableSeeder::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(BancoTableSeeder::class);
        $this->call(CajasTableSeeder::class);
        $this->call(VariablesTableSeeder::class);
        $this->call(BeneficiarioTableSeeder::class);
    }
}
