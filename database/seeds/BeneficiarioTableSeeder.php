<?php

use Illuminate\Database\Seeder;
Use App\Beneficiario;

class BeneficiarioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Beneficiario::create([
            'id' => '17123456',
            'nombre' => 'José Perez',
            'tipocta' => 'Corriente',
            'cta' => '3234567891234567',
            'correo' => 'jose@perez.com',
            'observacion' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. ',
            'banco_id' => '0105',
        ]);

        Beneficiario::create([
            'id' => '16123456',
            'nombre' => 'Ana Graterol',
            'tipocta' => 'Corriente',
            'cta' => '2234567891234567',
            'correo' => 'ana@graterol.com',
            'observacion' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. ',
            'banco_id' => '0102',
        ]);

        Beneficiario::create([
            'id' => '15123456',
            'nombre' => 'Juan Restrepo',
            'tipocta' => 'Ahorro',
            'cta' => '1234567891234567',
            'correo' => 'juan@restrepo.com',
            'observacion' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. ',
            'banco_id' => '0116',
        ]);
    }
}
