<?php

use Illuminate\Database\Seeder;
use App\Estado;

class EstadosTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Estado::create([
            'nombre' => 'Amazonas'
        ]);
        Estado::create([
            'nombre' => 'Anzoátegui'
        ]);
        Estado::create([
            'nombre' => 'Apure'
        ]);
        Estado::create([
            'nombre' => 'Aragua'
        ]);
        Estado::create([
            'nombre' => 'Barinas'
        ]);
        Estado::create([
            'nombre' => 'Bolívar'
        ]);
        Estado::create([
            'nombre' => 'Carabobo'
        ]);
        Estado::create([
            'nombre' => 'Cojedes'
        ]);
        Estado::create([
            'nombre' => 'Delta Amacuro'
        ]);
        Estado::create([
            'nombre' => 'Falcón'
        ]);
        Estado::create([
            'nombre' => 'Guárico'
        ]);
        Estado::create([
            'nombre' => 'Lara'
        ]);
        Estado::create([
            'nombre' => 'Mérida'
        ]);
        Estado::create([
            'nombre' => 'Miranda'
        ]);
        Estado::create([
            'nombre' => 'Monagas'
        ]);
        Estado::create([
            'nombre' => 'Nueva Esparta'
        ]);
        Estado::create([
            'nombre' => 'Portuguesa'
        ]);
        Estado::create([
            'nombre' => 'Sucre'
        ]);
        Estado::create([
            'nombre' => 'Táchira'
        ]);
        Estado::create([
            'nombre' => 'Trujillo'
        ]);
        Estado::create([
            'nombre' => 'Vargas'
        ]);
        Estado::create([
            'nombre' => 'Yaracuy'
        ]);
        Estado::create([
            'nombre' => 'Zulia'
        ]);
        Estado::create([
            'nombre' => 'Distrito Capital'
        ]);
        Estado::create([
            'nombre' => 'Dependencias Federales'
        ]);
    }
}