<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'id' => '17341248',
            'nombre' => 'Edgardo Mañez',
            'tlf' => '56979043703',
            'dir' => 'Chile',
            'nivel' => 'Administrador',
            'sucursal_id' => 'arau',
            'password' => bcrypt('binario'),
            
        ]);

        User::create([
            'id' => '15213792',
            'nombre' => 'Eduardo Rodriguez',
            'tlf' => '584166224684',
            'dir' => 'Araure',
            'nivel' => 'Administrador',
            'sucursal_id' => 'arau',
            'password' => bcrypt('15213792'),
        ]);

        User::create([
            'id' => '33333333',
            'nombre' => 'Usuario Auditoria',
            'tlf' => '5811111111',
            'dir' => 'Araure',
            'nivel' => 'Auditor',
            'sucursal_id' => 'arau',
            'password' => bcrypt('secret'),
        ]);

        User::create([
            'id' => '22222222',
            'nombre' => 'Usuario Sucursal',
            'tlf' => '5822222222',
            'dir' => 'Araure',
            'nivel' => 'Usuario',
            'sucursal_id' => 'arau',
            'password' => bcrypt('secret'),
        ]);

        factory(App\User::class)->times(100)->create();
    }
}
